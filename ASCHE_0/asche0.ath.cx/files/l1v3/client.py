import socket

###############################################
# Settings
###############################################
host = "l1v3.ath.cx"
port = 3113
sock = 0

###############################################
# ConnectToServer
###############################################
def ConnectToServer():
    global host, port, sock

    text  = "\n"
    text += "###############################################\n"
    text += "# l1v3 client v0.0.1                          #\n"
    text += "# (c) Time Studios 2008                       #\n"
    text += "###############################################\n"
    
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    #connecting
    text += "Connecting to %s : %i\n" % (host, port)

    try:
        sock.connect((host, port))
        sock.setblocking(0)
    except:
        text += "Error\n"
        text += "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n"
        return 0, text

    text += "Done\n"
    text += "###############################################\n"
    #end

    return 1, text

###############################################
# SendToServer
###############################################
def SendToServer(comm):
    global sock

    try:
        sock.send(comm)
    except:
        pass

###############################################
# SendToServer
###############################################
def ReceiveFromServer():
    global sock

    try:
        return sock.recv(1024)
    except:
        pass

###############################################
# DisconnectFromServer
###############################################
def DisconnectFromServer():
    global sock

    text = "Closing socket\n"
    sock.close()
    text += "Done\n"
    text += "###############################################\n"

    return text
