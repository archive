from OpenGL.GLUT import *
from engine import *

###############################################
# Settings
###############################################
Width  = 800
Height = 600

###############################################
# Start
###############################################
glutInit(sys.argv)
glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH)
glutInitWindowSize(Width, Height)
glutInitWindowPosition(0, 0)
glutCreateWindow("OpenGL demo")

#binding functions
glutDisplayFunc(DrawGLScene)
glutIdleFunc(DrawGLScene)
glutReshapeFunc(ReSizeGLScene)
glutKeyboardFunc(KeyPressed)

InitGL(Width, Height)
glutMainLoop()
