from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from time import *
from func import *
from client import *

###############################################
# Settings
###############################################
tmr = tmr2 = tmr3 = clock()
fps = Angle = 0

text = "Connect? (y/n)"
text2 = ">>>"
comm = ""
connect = 0

###############################################
# InitGL
###############################################
def InitGL(Width, Height):
    glClearColor(0.0, 0.0, 0.0, 0.0)
    glClearDepth(1.0)
    glDepthFunc(GL_LESS)
    glEnable(GL_DEPTH_TEST)
    glShadeModel(GL_FLAT)
    glEnable(GL_LIGHTING)
    glEnable(GL_LIGHT0)
    glEnable(GL_COLOR_MATERIAL)
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, float(Width) / float(Height), 0.1, 1000.0)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    gluLookAt(0.0, 0.0, -5.0, # Where
              0.0, 0.0, -4.0, # LookTo
              0.0, 1.0,  0.0) # Vector

###############################################
# ReSizeGLScene
###############################################
def ReSizeGLScene(Width, Height):
    if Height == 0: Height = 1

    glViewport(0, 0, Width, Height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, float(Width) / float(Height), 0.1, 1000.0)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    gluLookAt(0.0, 0.0, -5.0, # Where
              0.0, 0.0, -4.0, # LookTo
              0.0, 1.0,  0.0) # Vector

###############################################
# KeyPressed
###############################################
def KeyPressed(*args):
    global text, text2, connect, comm

    if not connect:
        if args[0] == "y": # Connect
            text    = "Connect? (y/n) y"
            result  = ConnectToServer()
            connect = result[0]
            text   += result[1]
            if not connect:
                text += "Reconnect? (y/n)"

        elif args[0] == "q": # Exit
            sys.exit()

    else:
        if (args[0] >= " " and args[0] <= "z"):
            comm  += args[0]
            text2 += args[0]

        elif args[0] == "\015": # Enter
            text2 = ">>>"

            SendToServer(comm)

            if comm == "exit": # Disconnect
                text += DisconnectFromServer()
                text += "Reconnect? (y/n)"
                connect = 0

            comm = ""

        elif args[0] == "\010": # BackSpace
            if len(text2) > 3:
                text2 = text2[:-1]
                comm  = comm[:-1]

###############################################
# DrawGLScene
###############################################
def DrawGLScene():
    global tmr, tmr2, tmr3, fps, Angle
    global text, text2

    tm = clock() - tmr3
    if tm >= 1:
        rez = ReceiveFromServer()
        if rez: text += rez
        tmr3 = clock()

    fps_time = clock() - tmr
    if fps_time >= 1: 
        glutSetWindowTitle("FPS = " + str(round(fps / fps_time)))
        fps  = 0
        tmr = clock()

    t = clock() - tmr2
    if t > 0.02:
        Angle = (Angle + t / 0.02) % 360
        tmr2 = clock()

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    #text
    glPushMatrix()
    glColor4f(3.0, 3.0, 3.0, 3.0)
    drawText(text, 2.5, 1.8, 0.0)
    glColor4f(3.0, 3.0, 3.0, 3.0)
    drawText(text2, 2.5, -1.0, 0.0)
    glPopMatrix()
    #end

    #cube
    glPushMatrix()
    glTranslatef(-1.0, 0.0, 0.0)
    glRotatef(Angle, 1.0, 0.0, 0.0)
    glRotatef(Angle, 0.0, 1.0, 0.0)
    glRotatef(Angle, 0.0, 0.0, 1.0)
    glColor4f(1.0, 0.0, 0.0, 1.0)
    glutSolidCube(1)
    glPopMatrix()
    #end

    #---------------------------------------------#
    glutSwapBuffers()
    
    fps += 1
