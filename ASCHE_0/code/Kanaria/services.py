# -*- coding: utf8 -*-
import urllib, string, random

###############################################
# ReadURL
###############################################
def ReadURL(url):
    try:
        u = urllib.urlopen(url)
        s = u.read()
        u.close()
    except:
        return None

    return s

###############################################
# ReplaceHTML
###############################################
def ReplaceHTML(text):
    text = string.replace(text, "<br>", "\n")
    text = string.replace(text, "<br />", "\n")
    text = string.replace(text, "\n\n", "\n")

    text = string.replace(text, '&quot;', '"')
    text = string.replace(text, "&lt;", "<")
    text = string.replace(text, "&gt;", ">")
    text = string.replace(text, "<b>", "")
    text = string.replace(text, "</b>", "")

    return text

###############################################
# GetBashQuote
###############################################
def GetBashQuote():
    s = ReadURL("http://bash.org.ru/random")
    if not s: return "can't get data"

    p1 = s.find('<a href="/quote/') + 16
    p2 = s.find('">', p1)
    num = s[p1:p2]

    p1 = s.find("<div>") + 5
    p2 = s.find("</div>", p1)
    s = unicode(s[p1:p2], "cp1251")

    return "#%s\n%s" %(num, ReplaceHTML(s))

###############################################
# GetNyashQuote
###############################################
def GetNyashQuote():
    s = ReadURL("http://nyash.org.ru")
    if not s: return "can't get data"

    p1 = s.find('margin-right: 10px;">')
    p1 = s.find('margin-right: 10px;">', p1 + 1) + 28
    count = s[p1:p1+4]

    num = random.randint(1, int(count))
    s = ReadURL("http://nyash.org.ru/quote.php?i=%i" %num)

    p1 = s.find("</div><br />")
    p1 = s.find("</div><br />", p1 + 1) + 12
    p2 = s.find("</div>", p1 + 1)
    s = unicode(s[p1:p2], "koi8-r")

    return "#%i\n%s" %(num, ReplaceHTML(s))

###############################################
# GetTaktoQuote
###############################################
def GetTaktoQuote():
    s = ReadURL("http://tak-to.ru/random")
    if not s: return "can't get data"

    p1 = s.find('text-decoration:none;" href=') + 30
    p2 = s.find("'>", p1)
    num = s[p1:p2]

    p1 = s.find("<div id='quote'>")
    p1 = s.find("<div id='quote'>", p1 + 1) + 23
    p2 = s.find("</div>", p1) - 5
    s = unicode(s[p1:p2], "cp1251")

    return "#%s\n%s" %(num, ReplaceHTML(s))

###############################################
# GetWeather
###############################################
def GetWeather():
    s = ReadURL("http://informer.gismeteo.ru/rss/26063.xml")
    if not s: return "can't get data"

    # time1
    p1 = s.find("<item>")
    p12 = s.find("</description>", p1)
    time1 = s[p1:p12]

    p1 = time1.find("<title>") + 7
    p2 = time1.find("</title>")
    city = time1[p1:p2]

    p1 = time1.find("<description>") + 13
    weather = time1[p1:]

    time1 = city + ": " + weather

    # time2
    p1 = s.find("<item>", p12)
    p22 = s.find("</description>", p1)
    time2 = s[p1:p22]

    p1 = time2.find("<title>") + 7
    p2 = time2.find("</title>")
    city = time2[p1:p2]

    p1 = time2.find("<description>") + 13
    weather = time2[p1:]

    time2 = city + ": " + weather

    # time3
    p1 = s.find("<item>", p22)
    p32 = s.find("</description>", p1)
    time3 = s[p1:p32]

    p1 = time3.find("<title>") + 7
    p2 = time3.find("</title>")
    city = time3[p1:p2]

    p1 = time3.find("<description>") + 13
    weather = time3[p1:]

    time3 = city + ": " + weather

    # time4
    p1 = s.find("<item>", p32)
    p42 = s.find("</description>", p1)
    time4 = s[p1:p42]

    p1 = time4.find("<title>") + 7
    p2 = time4.find("</title>")
    city = time4[p1:p2]

    p1 = time4.find("<description>") + 13
    weather = time4[p1:]

    time4 = city + ": " + weather


    return unicode("gismeteo.ru:\n" + time1 + "\n" + time2 + "\n" + time3 + "\n" + time4, "cp1251")

###############################################
# Decode
###############################################
def Decode(text, lang):
    english = u"QWERTYUIOP{}ASDFGHJKL:\"ZXCVBNM<>?qwertyuiop[]asdfghjkl;'zxcvbnm,./@#$^&"
    russian = u"ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ,йцукенгшщзхъфывапролджэячсмитьбю.\"№;:?"

    str = ""
    if lang == "en":
        for char in text:
            pos = russian.find(char)

            if pos != -1:
                str += english[pos]
            else:
                str += char

    elif lang == "ru":
        for char in text:
            pos = english.find(char)

            if pos != -1:
                str += russian[pos]
            else:
                str += char

    return str

###############################################
# GetHelp
###############################################
def GetHelp():
    return u'Я - кукла Kanaria: http://www.youtube.com/watch?v=Ex4qz8dghAo \n\
Доступные команды:\n\
\n\
"->help" это сообщение\n\
"->ping" <-pong (для теста)\n\
\n\
"->weather" погода от gismeteo\n\
\n\
"->bash" произвольная цитата с башорга\n\
"->nyash" произвольная цитата с няшорга\n\
"->takto" произвольная цитата от Кэпа :o)\n\
\n\
"->ru" декодирование предыдущего сообщения (ghbdtn -> привет)\n\
"->en" декодирование предыдущего сообщения (руддщ -> hello)\n\
"->ru text" декодирование текста\n\
"->en text" декодирование текста'
