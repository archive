#!/usr/bin/python

# JabberBot: A simple jabber/xmpp bot framework
# Copyright (c) 2007 Thomas Perl <thp@thpinfo.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Homepage: http://thpinfo.com/2007/python-jabberbot/
#
# Patched by ASCHE (rooms and other) <asche0@gmail.com>

import signal, xmpp, inspect, datetime, sys, os, re, time

class JabberBot:
    reconnectTime = 30
    commandPrefix = "bot_"

    #---------------------------------------------#
    def __init__(self, user, rooms, owner):
        signal.signal(signal.SIGTERM, sigTermCB)
        signal.signal(signal.SIGHUP,  sigHupCB)

        self.jid        = xmpp.JID(user[0])
        self.password   = user[1]
        self.res        = user[2]
        self.rooms      = rooms
        self.owner      = owner
        self.conn       = None
        self.__finished = False

        # import methods
        self.commands = {}
        for (name, value) in inspect.getmembers(self):
            if inspect.ismethod(value) and name.startswith(self.commandPrefix):
                self.commands[name[len(self.commandPrefix):]] = value

    #---------------------------------------------#
    def loadData(self):
        pass

    #---------------------------------------------#
    def log(self, msg):
        time = str(datetime.datetime.now()).split(".")[0]
        print "[%s] %s: %s" %(time, self.__class__.__name__, msg)
        sys.stdout.flush()

    #---------------------------------------------#
    def connect(self):
        if not self.conn:
            conn = xmpp.Client(self.jid.getDomain(), debug = [])

            if not conn.connect():
                self.log('unable to connect to server')
                return None

            if not conn.auth(self.jid.getNode(), self.password, self.res):
                self.log('unable to authorize with server')
                return None

            for room in self.rooms:
                conn.send(xmpp.Presence(to="%s/%s" %(room, self.res)))

            conn.RegisterHandler('message', self.messageCB)
            conn.RegisterHandler('presence', self.presenceCB)
            conn.sendInitPresence()
            self.conn = conn

    #---------------------------------------------#
    def exit(self, msg, restart = None):
        for room in self.rooms:
            self.leave(room)
        self.conn = None

        self.__finished = True
        self.log(msg)

        time.sleep(3)
        if restart: os.spawnv(os.P_NOWAIT, "./Yoko", [])

    #---------------------------------------------#
    def send(self, type, user, text, extra = ""):
        self.conn.send("<message to='%s' type='%s'><body>%s</body>%s</message>" %(user, type, text, extra))

    #---------------------------------------------#
    def join(self, room):
        if not room in self.rooms:
            self.conn.send(xmpp.Presence(to='%s/%s' %(room, self.res)))
            self.rooms.append(room)
            return True

    #---------------------------------------------#
    def leave(self, room):
        if room in self.rooms:
            self.conn.send(xmpp.Presence(to="%s/%s" %(room, self.res), typ="unavailable", status="offline"))
            self.rooms.remove(room)
            return True

    #---------------------------------------------#
    def messageCB(self, conn, mess):
        # just a history
        if mess.getTimestamp():
            return

        text = mess.getBody()
        type = mess.getType()
        mfrm = "%s" %(mess.getFrom())

        if not text:
            return

        # in groupchat handling only with prefix
        if type == "groupchat":
            if re.match("^%s. " %(self.res), text):
                text = re.findall("^%s. (.*)$" %(self.res), text)[0]
            else:
                return

        # parsing mfrm
        if not "/" in mfrm:
            return
        user, prefix = mfrm.split("/", 1)
        if user in self.rooms:
            owner = prefix == self.owner[1] ### msg from room ###
        else:
            owner = user == self.owner[0]   ### msg from jid ###
        if type == "groupchat":
            prefix += ", "                  ### groupchat => prefix ###
        else:
            user, prefix = mfrm, ""         ### chat => no prefix ###

        # parsing command
        if " " in text:
            cmd, args = text.split(" ", 1)
            args = args.split(" ")
        else:
            cmd, args = text, []

        # executing command
        if self.commands.has_key(cmd):
            result = self.commands[cmd](args, owner)
            if self.__finished: return
            if result:
                msg, extra = result
            else:
                msg, extra = "invalid syntax", ""
        else:
            msg, extra = "command not found", ""

        # replying
        if msg:
            self.send(type, user, "%s%s" %(prefix, msg), extra)

    #---------------------------------------------#
    def presenceCB(self, conn, pres):
        if (pres.getType() == "subscribe"):
            self.conn.send(xmpp.Presence(to=pres.getFrom(), typ="subscribed"))

    #---------------------------------------------#
    def process(self):
        while not self.__finished:
            try:
            ###
                if self.conn:
                    self.conn.Process(1)
                else:
                    self.connect()
                    if self.conn:
                        self.log("bot connected")
                    else:
                        self.log("could not connect to server")
                        time.sleep(self.reconnectTime)
            ###
            except xmpp.protocol.XMLNotWellFormed:
                self.log("reconnect (detected not valid XML)")
                self.conn = None
            except KeyboardInterrupt:
                self.exit("interrupted by keyboard")
            except SystemExit:
                self.exit("interrupted by SIGTERM")
            except ReloadData:
                self.log("interrupted by SIGHUP")
                self.loadData()

###############################################
def sigTermCB(signum, frame):
    raise SystemExit()

###############################################
class ReloadData(Exception):
    pass

###############################################
def sigHupCB(signum, frame):
    raise ReloadData()
