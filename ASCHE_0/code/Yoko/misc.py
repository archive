import os, random

###############################################
def loadFile(file):
    lines = []
    list  = []

    try:
        f = open(file, "r")
        lines = f.readlines()
        f.close()
    except:
        pass

    if lines:
        for line in lines:
            imgs = line.split(" ")
            imgs[2] = imgs[2][:-1]
            list.append(imgs)
        return list
    else:
        return None

###############################################
def loadLists(path):
    lists = {}

    for file in os.listdir(path):
        list = loadFile(path + "/" + file)

        if list:
            name = os.path.splitext(file)[0]
            lists[name] = list

    return lists

###############################################
def getImgXML(img_src, img_url):
    return "<html xmlns='http://jabber.org/protocol/xhtml-im'>" +\
           "<body xml:lang='en-US' xmlns='http://www.w3.org/1999/xhtml'>" +\
           "<a href='%s'><img src='%s' /></a>" %(img_src, img_url) +\
           "</body>" +\
           "</html>"

###############################################
def getRandomImgXML(comm, lists):
    listName = comm
    small    = None

    if not comm:
        listName = random.choice(lists.keys())
        if random.randint(0, 1) == 1:
            small = True
    else:
        if comm[-2:] == "_s":
            listName = comm[:-2]
            small = True

    try: list = lists[listName]
    except: return None

    imgs = random.choice(list)

    if small: img_url = imgs[2]
    else:     img_url = imgs[1]

    return getImgXML(imgs[0], img_url)

###############################################
def getTagsInfo(lists):
    sizes = ""
    tags  = ""

    for key in lists.keys():
        tags  += "%s, " %(key)
        sizes += "%s: %i\n" %(key, len(lists[key]))

    return tags[:-2], sizes[:-1]
