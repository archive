# -*- coding: cp1251 -*-
import urllib, string, random

###############################################
# koi8r_cp1251
###############################################
def koi8r_cp1251(text):
    from_cp = "koi8-r"
    to_cp   = "cp1251"
    return unicode(text, from_cp).encode(to_cp);

###############################################
# ReadURL
###############################################
def ReadURL(url):
    u = urllib.urlopen(url)
    s = u.read()
    u.close()

    return s

###############################################
# ReplaceHTML
###############################################
def ReplaceHTML(text):
    text = string.replace(text, "<br>", "\n")
    text = string.replace(text, "<br />", "\n")
    text = string.replace(text, '&quot;', '"')
    text = string.replace(text, "&lt;", "<")
    text = string.replace(text, "&gt;", ">")
    text = string.replace(text, "<b>", "")
    text = string.replace(text, "</b>", "")

    return text

###############################################
# GetBashQuote
###############################################
def GetBashQuote():
    s = ReadURL("http://bash.org.ru/random")

    p1 = s.find('<a href="/quote/') + 16
    p2 = s.find('">', p1)
    num = s[p1:p2]

    p1 = s.find("<div>") + 5
    p2 = s.find("</div>", p1)
    s = s[p1:p2]

    return "#%s\n%s" %(num, ReplaceHTML(s))

###############################################
# GetNyashQuote
###############################################
def GetNyashQuote():
    s = ReadURL("http://nyash.org.ru")

    p1 = s.find('margin-right: 10px;">')
    p1 = s.find('margin-right: 10px;">', p1 + 1) + 28
    count = s[p1:p1+4]

    num = random.randint(1, int(count))
    s = ReadURL("http://nyash.org.ru/quote.php?i=%i" %num)

    p1 = s.find("</div><br />")
    p1 = s.find("</div><br />", p1 + 1) + 12
    p2 = s.find("</div>", p1 + 1)
    s = koi8r_cp1251(s[p1:p2])

    return "#%i\n%s" %(num, ReplaceHTML(s))

###############################################
# GetTaktoQuote
###############################################
def GetTaktoQuote():
    s = ReadURL("http://tak-to.ru/random")

    p1 = s.find('text-decoration:none;" href=') + 30
    p2 = s.find("'>", p1)
    num = s[p1:p2]

    p1 = s.find("<div id='quote'>")
    p1 = s.find("<div id='quote'>", p1 + 1) + 23
    p2 = s.find("</div>", p1) - 5
    s = s[p1:p2]

    return "#%s\n%s" %(num, ReplaceHTML(s))

###############################################
# GetWeather
###############################################
def GetWeather():
    url = "http://informer.gismeteo.ru/rss/26063.xml"
    u = urllib.urlopen(url)
    s = u.read()

    # time1
    p1 = s.find("<item>")
    p12 = s.find("</description>", p1)
    time1 = s[p1:p12]

    p1 = time1.find("<title>") + 7
    p2 = time1.find("</title>")
    city = time1[p1:p2]

    p1 = time1.find("<description>") + 13
    weather = time1[p1:]

    time1 = city + ": " + weather

    # time2
    p1 = s.find("<item>", p12)
    p22 = s.find("</description>", p1)
    time2 = s[p1:p22]

    p1 = time2.find("<title>") + 7
    p2 = time2.find("</title>")
    city = time2[p1:p2]

    p1 = time2.find("<description>") + 13
    weather = time2[p1:]

    time2 = city + ": " + weather

    # time3
    p1 = s.find("<item>", p22)
    p32 = s.find("</description>", p1)
    time3 = s[p1:p32]

    p1 = time3.find("<title>") + 7
    p2 = time3.find("</title>")
    city = time3[p1:p2]

    p1 = time3.find("<description>") + 13
    weather = time3[p1:]

    time3 = city + ": " + weather

    # time4
    p1 = s.find("<item>", p32)
    p42 = s.find("</description>", p1)
    time4 = s[p1:p42]

    p1 = time4.find("<title>") + 7
    p2 = time4.find("</title>")
    city = time4[p1:p2]

    p1 = time4.find("<description>") + 13
    weather = time4[p1:]

    time4 = city + ": " + weather


    return "gismeteo.ru:\n" + time1 + "\n" + time2 + "\n" + time3 + "\n" + time4

###############################################
# Decode
###############################################
def Decode(text, lang):
    english = "QWERTYUIOP{}ASDFGHJKL:\"ZXCVBNM<>?qwertyuiop[]asdfghjkl;'zxcvbnm,./@#$^&"
    russian = "��������������������������������,��������������������������������.\"�;:?"

    str = ""
    if lang == "en":
        for char in text:
            pos = russian.find(char)

            if pos != -1:
                str += english[pos]
            else:
                str += char

    elif lang == "ru":
        for char in text:
            pos = english.find(char)

            if pos != -1:
                str += russian[pos]
            else:
                str += char

    return str
