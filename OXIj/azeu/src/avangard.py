# -*- coding: utf-8 -*-
###
#This file is part of azeu project
#
#azeu is automatic upload manager
#Copyright (C) 2007 Jan Trofimov (OXIj)
#
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#You can contact author by email jan.trof@gmail.com
###

import time
from utils import *
import avangard_api

timeout = 60
gateway_tries = 6

class File:
	_cookies = ''

	aid = 0
	name = ''
	size = 0
	description = ''
	date = 0

	counter = -1
	downloads = None

	deleted = 0

	def __init__(self, cookies, aid, name, size, date, counter = -1, description = ''):
		self._cookies = cookies
		self.aid = aid
		self.name = name
		self.size = size
		self.description = description
		self.date = date

		self.counter = counter
		self.downloads = {}

		self.deleted = 0

	def update(self):
		self.counter, self.downloads = avangard_api.file_stat(self._cookies, self.aid)
		tmp_name, self.description = avangard_api.file_info(self._cookies, self.aid)

		if (tmp_name != self.name):
			wlog('avangard', 'Error in names found! self => "' + self.name + \
					'" but avangard_api.info => "' + tmp_name + '"', 2)

	def delete(self):
		avangard_api.file_delete(self._cookies, self.aid)
		self.deleted = 1

	def __str__(self):
		return '    File: ' + str(self.aid) +\
		 '\n        Name       : ' + str(self.name) +\
		 '\n        Size       : ' + str(self.size) +\
		 '\n        Desc       : ' + str(self.description) +\
		 '\n        Death date : ' + str( time.strftime("%d.%m.%Y %H:%M", time.localtime(self.date)) ) +\
		 '\n        DCounter   : ' + str(self.counter) + '\n'

class Account:
	_cookies = None

	email = ''
	password = ''
	description = ''

	space = -1
	files = None

	def __init__(self, email = '', password = '', space= -1, description= ''):
		self.email = email
		self.password = password
		self.description = description
		self.space = space
		self.files = {}

	def append (self, aid, name, size, date, counter = -1, description = ''):
		self.files[aid] = File(self._cookies, aid, name, size, date, counter, description)

	def update(self, update_files = 0):
		wlog('avangard', 'Updating account info "' + self.email + '"...')
		
		self._cookies = avangard_api.cookies(self.email, self.password)
		space, array = avangard_api.profile(self._cookies)

		self.space = space
		oldfiles = self.files
		self.files = {}
		for one in array:
			counter = 0
			description = ''

			need_update = 1
			if (oldfiles.has_key(one)):
				counter = oldfiles[one].counter
				description = oldfiles[one].description
				need_update = 0

			self.append(one, array[one][0], array[one][1], array[one][2], counter, description)

			if (need_update or update_files):
				self.files[one].update()

		wlog('avangard', 'Updating account info done.')

	def upload (self, path, password = '', description = ''):
		wlog('avangard', 'Uploading file "' + path + '" with account "' + self.email + '"...')
		self.update()

		fsize = file_size(path)
		if ( fsize > self.space ):
			wlog('avangard', 'Error with free space.', 2)
			return None

		t_name = avangard_api.convert_name(file_basename(path))
		aid = avangard_api.file_upload(self._cookies, self.space, path, t_name)

		if ( aid == -1 ):
			step = 0
			while (step < gateway_tries):
				wlog('avangard', 'Not normal end of upload. Sleep and then check for uploaded file...')
				#sleep for $timeout secs before updating
				sleep(timeout)
		
				self.update()
			
				for one in self.files:
					if ( self.files[one].name == t_name ):
						aid = one
						break

				step += 1
		else:
			wlog('avangard', 'Normal end of upload.')
			self.update()

		if (aid == -1):
			wlog('avangard', 'File not found on server. Upload error.', 2)
			return None

		wlog('avangard', 'File found on server.')
		wlog('avangard', 'Confirming file info...')

		avangard_api.file_confirm(self._cookies, aid, password, description)
		self.files[aid].password = password
		self.files[aid].description = description

		wlog('avangard', 'Confirming file info done.')
		wlog('avangard', 'Uploading done.')

		return ('http://avangard.data.cod.ru/' + str(aid), t_name, fsize)

	def __str__(self):
		return 'Account: ' + str(self.email) +\
		 '\n    Password: ' + str(self.password) +\
		 '\n    Space   : ' + str(self.space) +\
		 '\n    Desc    : ' + str(self.description) +\
		 '\n    Files   : ' + str(len(self.files)) + '\n'

class Avangard:
	accounts = None
	def __init__(self):
		self.accounts = []

	def append (self, email, password, space = -1, description = ''):
		result = 0
		for acc in self.accounts:
			if (email == acc.email):
				result = 1
				break

		if (result == 0):
			self.accounts.append(Account(email, password, space, description))

	def find(self, space, strategy = 0):
		minacc = None
		for acc in self.accounts:
			if (acc.space == -1):
				acc.update()

			diff = acc.space - space
			if (diff == 0):
				minacc = acc
				break
			elif (diff > 0):
				if (minacc == None) or (\
				   (minacc.space - space) < diff ):
					minacc = acc
					if (strategy == 1):
						break
		return minacc

	def upload (self, path, password = '', description = '', strategy = 0):
		while ( 1 ):
			result = None

			condition = 'ok'

			acc = self.find(file_size(path), strategy)
			if (acc == None):
				condition = 'fail'
			else:
				result =  acc.upload(path, password, description)
				if (result == None):
					condition = 'retry'

			if (condition == 'ok'):
				return result
			elif (condition == 'fail'):
				return None
		#end while

