# -*- coding: utf-8 -*-
###
#This file is part of azeu project
#
#azeu is automatic upload manager
#Copyright (C) 2007 Jan Trofimov (OXIj)
#
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#You can contact author by email jan.trof@gmail.com
###

import sys, os, time
import sgetopt

from utils import *
import avangard

version_num = '0.3pre5'
version_string='azeu uploader ' + version_num + '\n\
Copyright (C) 2007 Jan Trofimov (OXIj)\n\
This is free software; see the source for copying conditions. There is NO\n\
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n'
help_string= version_string + '\n\
\n\
Input arguments = command line arguments which are not parsed as options.\
\n\
Options:\n\
\n\
-af\n\
--add-files\n\
    Upload files to avangard.data.cod.ru and add them to the base.\n\
\n\
    -s\n\
    --strategy\n\
        Uploading strategy.\n\
                0) Less space unused on accounts. Slower while updationg accounts.\n\
                1) More space unused on accounts. Faster while updationg accounts.\n\
\n\
    -p\n\
    --password\n\
        Password for uploaded files.\n\
\n\
    -d\n\
    --description\n\
        Description for uploaded files.\n\
\n\
\n\
-aa\n\
--add-accounts\n\
    Add accounts to the base.\n\
        email:password [ [free_space, -1] [decription] ]\n\
\n\
\n\
-la\n\
--list-accounts\n\
    List accounts (with info) which are in arguments list or all if arguments are empty list.\n\
\n\
\n\
-lf\n\
--list-files\n\
    List files (with info) which are in arguments list or all if arguments are empty list.\n\
\n\
\n\
-ra\n\
--remove-accounts\n\
    Remove accounts which are in arguments list from the base.\n\
\n\
\n\
-rf\n\
--remove-files\n\
    Remove files which are in arguments list from base and server.\n\
\n\
\n\
-i\n\
--input\n\
    Append file lines as arguments.\n\
\n\
\n\
-f\n\
--force\n\
    Force accounts check on start. Or if specified twice force full (accounts and files) check on start.\n\
\n\
\n\
-V\n\
--version\n\
    Show azeu version.\n\
\n\
\n\
-h\n\
--help\n\
    Show this help.\n\
\n\
\n\
Typical usage:\n\
    Upload some files:\n\
        azeu -af /some/file1 /some/file2\n\
    Upload some files from lists:\n\
        azeu -af -i file_with_list_of_some_files1 -i file_with_list_of_some_files2\n\
    Remove file with id 123456789:\n\
	azeu -rf 123456789\n\
    Remove files with ids from files:\n\
        azeu -rf -i file_with_list_of_some_file_ids1 -i file_with_list_of_some_file_ids2\n\
    Just force check without uploading something:\n\
        azeu -af -ff\n\
\n'

option_ideas = [
	['add-files',			'af',	0,	['add-files']],
		['strategy',			's',	1,	['add-files']],
		['password',			'p',	1,	['add-files']],
		['description',			'd',	1,	['add-files']],
	['add-accounts',		'aa',	0,	['add-accounts']],

	['list-accounts',		'la',	0,	['list-accounts']],
	['list-files',			'lf',	0,	['list-files']],

	['remove-accounts',		'ra',	0,	['remove-accounts']],
	['remove-files',		'rf',	0,	['remove-files']],

	['input',			'i',	1,	['add-files', 'add-accounts',\
							 'list-accounts', 'list-files',\
							 'remove-accounts', 'remove-files']],

	['force',			'f',	0,	['add-files', 'add-accounts',\
							 'list-accounts', 'list-files',\
							 'remove-accounts', 'remove-files']],
	['help', 			'h',	0,	['help']],
	['version', 			'V',	0,	['version']],
]

def cross (array1, array2):
	result = []
	for one in array1:
		for two in array2:
			if (one == two):
				result.append(one)

	return result

def inside(where, what):
	for one in where:
		if (one[0] == what):
			return 1
	return 0

#default option values
default_accounts= os.environ['HOME'] + '/.azeu/accounts.conf'
default_files= os.environ['HOME'] + '/.azeu/files.conf'

#main 'plugin'
ava = avangard.Avangard()

#option parceing
optlist, optargs = None, None
try:
	optlist, arguments = sgetopt.sgetopt(sys.argv[1:], option_ideas )

	rclass = []
	if (len(optlist) > 0):
		rclass = optlist[0][0][3]
		for one in optlist[1:]:
			rclass = cross(rclass, one[0][3])

	if (len(rclass) != 1):
		raise sgetopt.SGetoptError()

	rclass = rclass[0]
except sgetopt.SGetoptError:
	wlog('core', 'Wrong parameters.', 3)
	sys.exit(1)

if (rclass == 'version'):
	print version_string
	sys.exit(0)
elif (rclass == 'help'):
	print help_string
	sys.exit(0)

#config reading
t_file = open(default_accounts, 'rb')
for line in t_file.readlines():
	if (line[-1:] == '\n'):
		line = line [:-1]
	if ( line == '' ):
		continue

	tmp = line.split(' ', 2)
	ep = tmp[0].split(':')
	email, password = ep[0], ep[1]

	space = -1
	if ((len(tmp) > 1) and (tmp[1] != '')):
		space = int(tmp[1])

	description = ''
	if (len(tmp) > 2):
		description = tmp[2]

	ava.append(email, password, space, description)
t_file.close()

t_file = open(default_files, 'rb')
for line in t_file.readlines():
	if (line[-1:] == '\n'):
		line = line [:-1]
	if ( line == '' ):
		continue

	email, aid, name, size, date, counter, description  = line.split(':', 6)
	
	for acc in ava.accounts:
		if (acc.email == email):
			acc.append(int(aid), name, int(size), float(date), int(counter), description)

			if ( float(date) <= time.time() ):
				acc.space = -1

			break
t_file.close()

#prepareing values
password = ''
description = 'uploaded with azeu'
strategy = 0

force = 0

for opt in optlist:
	name, value = opt[0][0], opt[1]

	if ( name == 'input'):
		t_file = open(value, 'rb')
		for line in t_file.readlines():
			if (line[-1:] == '\n'):
				line = line [:-1]
			if (line == ''):
				continue
			arguments.append(line)
		t_file.close()
	elif ( name == 'force'):
		force += 1
	elif ( name == 'password' ):
		password = value
	elif ( name == 'description' ):
		description = value
	elif ( name == 'strategy' ):
		strategy = int(value)
		if ( (strategy > 1) or (strategy < 0) ):
			strategy = 0

#updating
if (force):
	for acc in ava.accounts:
		if ((acc.space == -1) or (force > 1)):
			need = 0
			if (force > 2):
				need = 1
			acc.update(need)

#doing things
if (rclass == 'add-files'):
	wlog('core', 'add-files mode.')

	cancelled = 0
	for path in arguments:
		res = None

		if (cancelled == 0):
			try:
				res = ava.upload(path, password, description, strategy)
			except KeyboardInterrupt:
				cancelled = 1

		if (res != None):
			wlog('core', 'File ' + str(res[0]) + ' ("' + str(res[1]) + '") uploaded to server and added to the base.')
			wout('success ' + str(res[0]) + ' ' + str(res[2])  + ' ' + str(res[1]))
		else:
			wout('fail ' + path)

elif (rclass == 'add-accounts'):
	wlog('core', 'add-accounts mode.')
	for account in arguments:
		tmp = account.split(' ', 2)
		ep = tmp[0].split(':')
		email, password = ep[0], ep[1]

		space = -1
		if ((len(tmp) > 1) and (tmp[1] != '')):
			space = int(tmp[1])

		description = ''
		if (len(tmp) > 2):
			description = tmp[2]

		wlog('core', 'Account "' + str(email) + '" with password "' + str(password) + '" added to the base.')
		ava.append(email, password, space, description)

elif (rclass == 'list-accounts'):
	wlog('core', 'list-accounts mode.')
	for acc in ava.accounts:
		if ( (len(arguments) == 0) or (str(acc.email) in arguments) ):
			wout(str(acc))

elif (rclass == 'list-files'):
	wlog('core', 'list-files mode.')
	for acc in ava.accounts:
		acc_printed = 0
		for one in acc.files:
			if ( (len(arguments) == 0) or (str(one) in arguments) ):
				if (acc_printed == 0):
					wout(str(acc))
					acc_printed = 1
				wout(str(acc.files[one]))

elif (rclass == 'remove-accounts'):
	wlog('core', 'remove-accounts mode.')
	for acc in ava.accounts[:]:
		if (str(acc.email) in arguments):
			wlog('core', 'Account "' + str(acc.email) + '" removed from the base.')
			ava.accounts.remove(acc)

elif (rclass == 'remove-files'):
	wlog('core', 'remove-files mode.')

	acc_to_update = []
	for acc in ava.accounts:
		for one in acc.files:
			if ( str(one) in arguments ):
				if (acc not in acc_to_update):
					acc_to_update.append(acc)

	for acc in acc_to_update:
		acc.update()

	for acc in ava.accounts:
		for one in acc.files:
			if ( str(one) in arguments ):
				wlog('core', 'File ' + str(acc.files[one].aid) + ' ("' + str(acc.files[one].name) + '") removed from base and server.')
				acc.files[one].delete()

	sleep(avangard.timeout)

	for acc in acc_to_update:
		acc.update()

t_afile = open(default_accounts, 'wb')
t_bfile = open(default_files, 'wb')
for acc in ava.accounts:
	print >> t_afile,  str(acc.email) + ':' +   str(acc.password) + ' ' + str(acc.space) + ' ' +  str(acc.description)
	for one in acc.files:
		f = acc.files[one]
		if (not f.deleted):
			print >> t_bfile, str(acc.email) + ':' + str(f.aid) + ':' +  str(f.name) + ':' +\
					  str(f.size) + ':' +\
					  str(f.date) + ':' + str(f.counter) + ':' +  str(f.description)
t_bfile.close()
t_afile.close()

wlog('core', 'All done.')
