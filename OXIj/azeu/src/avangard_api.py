# -*- coding: utf-8 -*-
###
#This file is part of azeu project
#
#azeu is automatic upload manager
#Copyright (C) 2007 Jan Trofimov (OXIj)
#
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#You can contact author by email jan.trof@gmail.com
###

import sys, os, time
from utils import *

user_agent = 'Mozilla/5.0 (X11; U; Linux i686; ru; rv:1.8.1.2) Gecko/20060601 Firefox/2.0.0.2'

page_timeout = 60
page_limit = 1024

upload_timeout = 420
upload_limit = 1024

def convert_name (name):
	newname = ''
	for l in name:
		if ( ( 'A' <= l <= 'Z' ) or
		     ( 'a' <= l <= 'z' ) or
		     ( '0' <= l <= '9' ) or
		     ( l in ['.', '_'] ) ):
			newname += l
		else:
			newname += '_'

	return newname

def make_query (query, write_in = '', stderr_out = 0):
	(cin, cout, cerr) = os.popen3 (query)

	cin.write(write_in)
	cin.close()

	if ( stderr_out ):
		cerrl = ""
		while ( 1 ):
			cerrl = cerr.read(1)
			if ( cerrl != ""):
				wraw(cerrl)
			else:
				break
	
	tup = os.wait()

	text = cout.read()

	cout.close()
	cerr.close()

	return (tup[1], text)

def cookies (email = '', password = ''):
	text = ''
	retcode = 1
	while ( retcode != 0 ):
		retcode, text = make_query ('curl --user-agent "' + user_agent + '"\
						     -y ' + str(page_timeout) + ' -Y ' + str(page_limit) + ' \
						     --output /dev/null\
						     --dump-header /dev/stdout\
						     --form "email=' + email + '"\
						     --form "password=' + password + '"\
						     "https://id.cod.ru/auth"')
	return text

def is_authorized(cookies):
	if ( cookies.find("auth=YES;") != -1 ):
		return 1
	else:
		return 0

def profile (cookies):
	append = ''
	if ( is_authorized(cookies) ):
		append = 'profile/'

	text = ''
	retcode = 1
	while ( retcode != 0 ):
		retcode, text = make_query ('curl --user-agent "' + user_agent + '"\
						     -y ' + str(page_timeout) + ' -Y ' + str(page_limit) + ' \
						     --cookie /dev/stdin\
						     "http://avangard.data.cod.ru/' + append + '"' , cookies)

	#максимальный размер
	start = text.find("value=",text.find("maxFileSize")) + 6
	stop = text.find(">", start)
	space = int(text[start:stop].strip())

	thebestmark = text.find("<!-- THE BEST -->")

	files = {}
	start = text.find("BlkCom")
	while ( start != -1 ) and (start < thebestmark):
		#id
		start = text.find("&nbsp;", start) + 6
		start = text.find("avangard.data.cod.ru/", start) + 21
		stop = text.find("\"", start)
		aid = int(text[start:stop])

		#имя файла
		start = stop + 2
		stop = text.find("</a>", start)
		name = text[start:stop]
			
		#размер
		start = stop + 1
		start = text.find(": ", start) + 2
		stop = text.find(",", start)
		size = int(text[start:stop-2])*1024

		#дата смерти
		start = stop + 1
		start = text.find(": ", start) + 2
		stop = text.find(",", start)
		date = time.mktime(time.strptime(text[start:stop], "%d.%m.%Y %H:%M"))

		files[aid] = (name, size, date)

		start = text.find("BlkCom", stop + 1)

	return (space, files)

def file_stat (cookies, aid):
	text = ''
	retcode = 1
	while ( retcode != 0 ):
		retcode, text = make_query ('curl --user-agent "' + user_agent + '"\
						     -y ' + str(page_timeout) + ' -Y ' + str(page_limit) + ' \
						     --cookie /dev/stdin\
						     "http://avangard.data.cod.ru/profile/?action=showStat&id='\
						     + str(aid) + '"', cookies )

	#число скачиваний
	start = text.find(": ", text.find("BlkLtT"))
	start = text.find("<b>", start) + 3
	stop = text.find("<", start)
	download_number = text[start:stop].strip()

	downloads = {}
	start = text.find("<table", start) + 6
	start = text.find("<tr><td><nobr>", start)
	while ( start != -1 ):
		#date
		start = start + 14
		stop = text.find("</nobr></td>", start)
		date = time.mktime(time.strptime(text[start:stop], "%Y.%m.%d-%H:%M:%S"))

		#ip
		start = stop + 1
		start = text.find("<td>", start) + 4
		stop = text.find("</td></tr>", start)
		ip = text[start:stop]

		downloads[date] = ip

		start = text.find("<tr><td><nobr>", stop + 1)

	return (download_number, downloads)

def file_delete (cookies, aid):
	text = ''
	retcode = 1
	while ( retcode != 0 ):
		retcode, text = make_query ('curl --user-agent "' + user_agent + '"\
						     -y ' + str(page_timeout) + ' -Y ' + str(page_limit) + ' \
						     --cookie /dev/stdin\
						     "http://avangard.data.cod.ru/profile/?action=deleteFile&id='\
						     + str(aid) + '"', cookies )

def file_upload (cookies, space, path, name):
	text = ''
	retcode = 1
	while ( retcode != 0 ):
		retcode, text = make_query ('curl --user-agent "' + user_agent + '"\
						     -y ' + str(upload_timeout) + ' -Y ' + str(upload_limit) + ' \
						     --form "maxFileSize=' + str(space) + '"\
						     --form "action=storeFile"\
						     --form "sfile=@' + path + ';filename=' + name + '"\
						     --form "agree=1"\
						     --form "x=56"\
						     --form "y=12"\
						     --cookie /dev/stdin\
						     http://avangard.data.cod.ru/put/', cookies , 1)
	#id
	start = text.find("number")
	if (start == -1):
		return -1

	start = text.find("value=", start) + 6
	stop = text.find(">", start)
	aid = int(text[start:stop])

	return aid

def file_confirm (cookies, aid, password, description):
	text = ''
	retcode = 1
	while ( retcode != 0 ):
		retcode, text = make_query ('curl --user-agent "' + user_agent + '"\
						     -y ' + str(page_timeout) + ' -Y ' + str(page_limit) + ' \
						     --form "action=confirmStore"\
						     --form "number=' + str(aid) + '"\
						     --form "password=' + password +'"\
						     --form "description=' + description + '"\
						     --form "catalogue="\
						     --cookie /dev/stdin\
						     http://avangard.data.cod.ru/put/', cookies )

def file_info (cookies, aid):
	text = ''
	retcode = 1
	while ( retcode != 0 ):
		retcode, text = make_query ('curl --user-agent "' + user_agent + '"\
						     -y ' + str(page_timeout) + ' -Y ' + str(page_limit) + ' \
						     --cookie /dev/stdin\
						     "http://avangard.data.cod.ru/profile/?action=editFile&id='\
						     + str(aid) + '"', cookies )

	#имя
	start = text.find("name=name", text.find("BlkLtT"))
	start = text.find("value=\"", start) + 7
	stop = text.find("\">", start)
	name = text[start:stop]

	#описание
	start = stop + 1
	start = text.find("name=description", start)
	start = text.find(">", start) + 1
	stop = text.find("</textarea>", start)
	description = text[start:stop]

	return (name, description)

def file_update (cookies, aid, name, password, description):
	text = ''
	retcode = 1
	while ( retcode != 0 ):
		retcode, text = make_query ('curl --user-agent "' + user_agent + '"\
						     -y ' + str(page_timeout) + ' -Y ' + str(page_limit) + ' \
						     --form "action=updateFile"\
						     --form "id=' + str(aid) + '"\
						     --form "name=' + name + '"\
						     --form "description=' + description + '"\
						     --form "variant=password"\
						     --form "password=' + password +'"\
						     --form "catalogue="\
						     --cookie /dev/stdin\
						     http://avangard.data.cod.ru/profile/', cookies )

