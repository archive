# -*- coding: utf-8 -*-
###
#This file is part of azeu project
#
#azeu is automatic upload manager
#Copyright (C) 2007 Jan Trofimov (OXIj)
#
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#You can contact author by email jan.trof@gmail.com
###

import sys, os, time

out_stream = sys.stdout
log_stream = sys.stderr

def wlog(module, message, level = 0):
	levstr = 'INFO'
	if (level == 1):
		levstr = 'WARNING'
	elif (level == 2):
		levstr = 'ERROR'
	elif (level == 3):
		levstr = 'PANIC'

	print >> log_stream, sys.argv[0] + ':' + module + ': [' + levstr + '] ' + message
	log_stream.flush()

def wraw (message):
	if (log_stream.isatty()):
		log_stream.write(message)

def wout (message):
	print >> out_stream, message
	out_stream.flush()

def sleep (stime):
	t_time = time.time()
	stop_time = t_time + stime
	while (1):
		t_time = time.time()
		if (t_time >= stop_time):
			break
		time.sleep(stop_time - t_time)

def file_size(path):
	f = open(path, 'rb')
	f.seek(0,2)
	size = f.tell()
	f.close()
	return size

def file_basename(path):
	return os.path.basename(path)
