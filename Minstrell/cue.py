import re, sys

WS = '[ \t]'
NONWS = '[^ \t]'
LB = '^'+WS+'*'
LE = WS+'*$'
SWS = WS+'+'
STRING = "('([^']|\\')*'|\"([^\"]|\\\")*\"|("+NONWS+'+))'
NUMBER = '([0-9]+)'
FILE_FORMAT = '(BINARY|MOTOROLA|AIFF|WAVE|MP3)'
TRACK_MODE = '(AUDIO|MODE1_2048|MODE1_2352|MODE2_2048|MODE2_2332|MODE2_2336|MODE2_2342|MODE2_2352)'
TRACK_FLAG = '(PRE|DCP|FOUR_CH|SCMS)'
TRACK_FLAGS = '('+TRACK_FLAG+WS+')*'+TRACK_FLAG
TIME = '('+NUMBER+':'+NUMBER+':'+NUMBER+')'

REM   = re.compile(LB+'REM.*$')
EMPTY = re.compile('^'+WS+'*$')
CATALOG    = re.compile(LB+'CATALOG'   +WS+STRING+LE)
CDTEXTFILE = re.compile(LB+'CDTEXTFILE'+WS+STRING+LE)

TITLE      = re.compile(LB+'TITLE'     +WS+STRING+LE)
PERFORMER  = re.compile(LB+'PERFORMER' +WS+STRING+LE)
SONGWRITER = re.compile(LB+'SONGWRITER'+WS+STRING+LE)
COMPOSER   = re.compile(LB+'COMPOSER'  +WS+STRING+LE)
ARRANGER   = re.compile(LB+'ARRANGER'  +WS+STRING+LE)
MESSAGE    = re.compile(LB+'MESSAGE'   +WS+STRING+LE)
DISC_ID    = re.compile(LB+'DISC_ID'   +WS+STRING+LE)
GENRE      = re.compile(LB+'GENRE'     +WS+STRING+LE)
TOC_INFO1  = re.compile(LB+'TOC_INFO1' +WS+STRING+LE)
TOC_INFO2  = re.compile(LB+'TOC_INFO2' +WS+STRING+LE)
UPC_EAN    = re.compile(LB+'UPC_EAN'   +WS+STRING+LE)
ISRC       = re.compile(LB+'ISRC'      +WS+STRING+LE)
SIZE_INFO  = re.compile(LB+'SIZE_INFO' +WS+STRING+LE)

FILE = re.compile(LB+'FILE'+WS+STRING+WS+FILE_FORMAT+LE)

TRACK = re.compile(LB+'TRACK'+WS+NUMBER+WS+TRACK_MODE+LE)
FLAGS = re.compile(LB+'FLAGS'+WS+TRACK_FLAGS+LE)
TRACK_ISRC = re.compile(LB+'TRACK_ISRC'+WS+STRING+LE)
PREGAP = re.compile(LB+'PREGAP'+WS+TIME+LE)
INDEX  = re.compile(LB+'INDEX'+WS+NUMBER+WS+TIME+LE)
POSTGAP = re.compile(LB+'POSTGAP'+WS+TIME+LE)

class CREUser:
    def match(self, cre, s):
        self.m = cre.match(s)
        return self.m

class CueTime:
    minutes = 0
    seconds = 0
    frames  = 0
    def __init__(self, m, s, f):
        self.minutes = m
        self.seconds = s
        self.frames  = f
    def is_zero(self):
        return minutes == 0 and seconds == 0 and frames == 0 
    def __str__(self)
        return '%02d:%02d:%02d' % (self.minutes, self.seconds, self.frames)

class CueCommonData:
    title = ''
    performer  = ''
    songwriter = ''
    composer = ''
    arranger = ''
    message  = ''
    disc_id  = ''
    genre = ''
    toc_info1 = ''
    toc_info2 = ''
    upc_ean = ''
    isrc = ''
    size_info = ''
    file = ''
    file_format = ''
    def __to_string_list(self):
        ss = []
        if self.title:
            ss.append('TITLE "%s"' % self.title)
        if self.performer:
            ss.append('PERFORMER "%s"'  % self.performer)
        if self.songwriter:
            ss.append('SONGWRITER "%s"' % self.songwriter)
        if self.composer:
            ss.append('COMPOSER "%s"' % self.composer)
        if self.arranger:
            ss.append('ARRANGER "%s"' % self.arranger)
        if self.message:
            ss.append('MESSAGE "%s"'  % self.message)
        if self.disc_id:
            ss.append('DISC_ID "%s"'  % self.disc_id)
        if self.genre:
            ss.append('GENRE "%s"' % self.genre)
        if self.toc_info1:
            ss.append('TOC_INFO1 "%s"' % self.toc_info1)
        if self.toc_info2:
            ss.append('TOC_INFO2 "%s"' % self.toc_info2)
        if self.upc_ean:
            ss.append('UPC_EAN "%s"' % self.upc_ean)
        if self.isrc:
            ss.append('ISRC "%s"' % self.isrc)
        if self.size_info:
            ss.append('SIZE_INFO "%s"' % self.size_info)
        if self.file and self.file_format:
            ss.append('FILE "%s" %s' % (self.file, self.file_format))
        return ss

class CueTrack(CueCommonData):    
    track_isrc = '' 
    pregap  = CueTime(0,0,0)
    postgap = CueTime(0,0,0)
    flags = []
    indexes = []
    def __init__(self, num, mode):
        self.num  = num
        self.mode = mode
    def add_flag(self, flag):
        self.flags.append(flag)
    def add_index(self, num, time):
        self.indexes.append((num, time))
    def __to_string_list(self):
        ss = []
        ss.append('TRACK %02d %s' % (self.num, self.mode))
        if self.track_isrc:
            ss.append('TRACK_ISRC "%s"' % self.track_isrc)
        if not self.pregap.is_zero():
            ss.append('PREGAP '+str(self.pregap))
        if not self.postgap.is_zero():
            ss.append('POSTGAP '+str(self.postgap))
        if self.flags:
            ss.append('FLAGS '+self.flags.join(' '))
        if self.indexes:
            for index in self.indexes:
                ss.append('INDEX %02d %s' % (index[0], str(index[1])))
        ss = ss + self.CueCommonData.__to_string_list()
        return ss

class CueSheet(CueCommonData):
    catalog = ''
    cdtextfile = ""
    tracks = []        
    def add_track(self, track):
        self.tracks.append(track)
    def to_string(self, sep):
        ss = []
        if self.catalog:
            ss.append('CATALOG '+self.catalog)
        if self.cdtextfile:
            ss.append('CDTEXTFILE '+self.cdtextfile)
        ss = ss + self.__to_string_list('')
        for track in tracks:
            ss = ss + track.__to_string_list()
        return ss.join(sep)
    
def match_cdtext(s, o):
    u = CREUser()
    if u.match(TITLE, s):
        o.title = u.m.group(1)
    elif u.match(PERFORMER, s):
        o.performer = u.m.group(1)
    elif u.match(SONGWRITER, s):
        o.songwriter = u.m.group(1)
    elif u.match(COMPOSER, s):
        o.composer = u.m.group(1)
    elif u.match(ARRANGER, s):
        o.arranger = u.m.group(1)
    elif u.match(MESSAGE, s):
        o.message = u.m.group(1)
    elif u.match(DISC_ID, s):
        o.disc_id = u.m.group(1)
    elif u.match(GENRE, s):
        o.genre = u.m.group(1)
    elif u.match(TOC_INFO1, s):
        o.toc_info1 = u.m.group(1)
    elif u.match(TOC_INFO2, s):
        o.toc_info2 = u.m.group(1)
    elif u.match(UPC_EAN, s):
        o.upc_ean = u.m.group(1)
    elif u.match(ISRC, s):
        o.isrc = u.m.group(1)
    elif u.match(SIZE_INFO, s):
        o.size_info = u.m.group(1)
    else:
        return False
    return True

def match_file(s, o):
    u = CREUser()
    if u.match(FILE, s):
        o.file = u.m.group(1)
        o.file_format = u.m.group(2)
    else:
        return False
    return True

def parse_string_list(slist):
    sheet = CueSheet()
    global_part = True
    u = CREUser()
    for s in slist:
        if global_part:
            if u.match(CATALOG, s):
                sheet.catalog = u.m.group(1)        
            elif u.match(CDTEXTFILE, s):
                sheet.cdtextfile = u.m.group(1)
            elif match_cdtext(s, sheet):
                pass
            elif match_file(s, sheet):
                pass
            elif u.match(TRACK, s):
                global_part = False
                track = CueTrack(int(u.m.group(1)), u.m.group(2))
            elif u.match(REM, s):
                pass
            elif u.match(EMPTY, s):
                pass
            else:
                print 'global error at %s' % s
        else:
            if u.match(TRACK, s):
                sheet.add_track(track)
                track = CueTrack(int(u.m.group(1)), u.m.group(2))
            elif match_cdtext(s, track):
                pass
            elif u.match(FLAGS, s):
                track.add_flag(u.m.group(1))
            elif u.match(TRACK_ISRC, s):
                track.track_isrc = u.m.group(1)
            elif u.match(PREGAP, s):
                track.pregap(CueTime(u.m.group(1)))
            elif u.match(INDEX, s):
                time = CueTime(int(u.m.group(3)),int(u.m.group(4)),int(u.m.group(5)))
                track.add_index(int(u.m.group(1)), time)
            elif u.match(POSTGAP, s):
                track.postgap = CueTime(u.m.group(1))
            elif match_file(s, track):
                pass
            elif u.match(REM, s):
                pass
            elif u.match(EMPTY, s):
                pass
            else:
                print 'track error at %s' % s
    return sheet    

def parse_file(filename,encoding):
    f = open(filename, 'r')
    data = f.read()
    udata = unicode(data, encoding)
    return parse_string_list(re.split('\r|\n', udata))