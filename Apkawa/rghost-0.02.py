#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Консольная заливка на rghost.ru
'''
import urllib, os, tempfile, httplib
import optparse
from re import findall
from sys import argv
from tempfile import TemporaryFile


USER_AGENT = 'Mozilla/5.0 (X11; U; Linux i686; ru-RU; rv:1.9.0.2pre) Gecko/2008072703 Firefox/3.0.2pre (Swiftfox)'
URL = 'http://rghost.ru/'

class Rghost:
    def __init__(self, argv):
        self.curl = 'curl -b "%s" \
                    -F authenticity_token=%s \
                    -F file=@"%s" \
                    -F commit="Отправить"  http://muon.rghost.ru/files'
        options, files = self.optparse(argv)
        #print options, files
        self.files = files
        self.email = options.email
        self.passwd = options.passwd

        #self.guest = self.email and 0 or 1

        if self.email: self.guest = 0
        else: self.guest = 1

        self.tmp = TemporaryFile(prefix='rghost_')

    def optparse(self, argv):
        parser = optparse.OptionParser()
        parser.add_option('-u','--user', action='store', type='string', dest='email')
        parser.add_option('-p','--password', action='store', type='string', dest='passwd')
        return parser.parse_args(argv)

    def main(self):
        for f in self.files:
            self.upload(f)

        self.tmp.seek(0)
        print '\n',self.tmp.read()


    def session(self):
        s = urllib.urlopen(URL)
        self.s_cookie = s.headers.getheader('Set-Cookie')
        self.s_token = findall('input name="authenticity_token" type="hidden" value="(.*?)"',s.read())[0]

        if not self.guest:
            values = {
                        'authenticity_token': self.s_token,
                        'return_to':URL,
                        'email':self.email,
                        'password':self.passwd,
                        'commit':'Войти',
                       }

            conn = httplib.HTTPConnection( 'rghost.ru' )
            conn.request('POST','/profile/login', urllib.urlencode(values) , {'Cookie':self.s_cookie})
            respone = conn.getresponse()
            self.s_cookie = respone.getheader('set-cookie')

            '''
            t = urllib.FancyURLopener()
            t.addheader('Cookie',self.s_cookie)
            login = t.open(URL+'profile/login',urllib.urlencode(values))
            print login.info()
            #self.s_cookie = login.headers.getheader('Set-Cookie')
            #self.s_token = findall('input name="authenticity_token" type="hidden" value="(.*?)"',login.read())[0]
            '''

    def upload(self,filepath):
        tsize = float(os.stat(filepath).st_size)
        if tsize > 1024:
            if tsize/1024 > 1024:
                size = '%3.2f%s'%((tsize/1024)/1024 , 'M')
            else:
                size ='%3.2f%s'%( tsize/1024 , 'K')
        else:
            size ='%3.0f'% tsize
        filename = os.path.split(filepath)[1]
        #print self.s_cookie
        #print self.s_token
        #print filepath
        #print os.popen(self.curl%( self.s_cookie, self.s_token, filepath)).read()
        curl_cmd = self.curl%( self.s_cookie, self.s_token, filepath)
        ##print curl_cmd
        result = os.popen( curl_cmd ).read()
        #print result

        self.tmp.write('%s%s %s %s\n'%\
                (URL,findall('"http://rghost.ru/(.*?)"', result)[0],
                    filename,
                    size))

if __name__ == '__main__':
    rg = Rghost(argv[1:])
    rg.session()
    rg.main()


