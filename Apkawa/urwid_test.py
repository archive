# -*- coding: utf-8 -*-
import urwid.curses_display
import urwid

class Conversation(object):
	def __init__(self):
		self.items = urwid.SimpleListWalker(
			[self.new_question()])
		self.listbox = urwid.ListBox(self.items)
		instruct = urwid.Text("Press F8 to exit.")
		header = urwid.AttrWrap( instruct, 'header' )
		self.top = urwid.Frame(self.listbox, header)

	def main(self):
		self.ui = urwid.curses_display.Screen()
		self.ui.register_palette([
			('header', 'black', 'dark cyan', 'standout'),
			('I say', 'default', 'default', 'bold'),
			])
		self.ui.run_wrapper( self.run )
	
	def run(self):
		size = self.ui.get_cols_rows()

		while True:
			self.draw_screen( size )
			keys = self.ui.get_input()
			if "f8" in keys:
				break
			for k in keys:
				if k == "window resize":
					size = self.ui.get_cols_rows()
					continue
				self.top.keypress( size, k )
			if keys:
				name = self.items[0].edit_text
				self.items[1:2] = [self.new_answer(name)]
	
	def draw_screen(self, size):
		canvas = self.top.render( size, focus=True )
		self.ui.draw_screen( size, canvas )

	def new_question(self):
		return urwid.Edit(('I say',"What is your name?\n"))

	def new_answer(self, name):
		return urwid.Text(('I say',"Nice to meet you, "+name+"\n"))

Conversation().main()
