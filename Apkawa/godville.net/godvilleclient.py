#!/usr/bin/python
# -*- coding: utf-8 -*-
###
#Client app for zero player game godville.net
#Copyright (C) 2009 Apkawa
#
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#You can contact author by email apkawa@gmail.com
###
import httplib, urllib, urllib2
import json
import re

class MyRedirectHandler(urllib2.HTTPRedirectHandler):
    def http_error_302(self, req, fp, code, msg, headers):
        pass
urllib2_30x = urllib2.build_opener( MyRedirectHandler )


class ParsePage:
    cookie = None
    def __init__(self):
        import html5lib
        self.headers= {
                'User-Agent':'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.1) Gecko/20090710 Firefox/3.5 (Swiftfox) GTB5',
                'Cookie':'',
                }
        pass
    def auth(self, username, password):
        url = 'http://www.godville.net/login/login'
        __headers = self.headers
        __headers.update( {'Referer':'http://www.godville.net/login'} )
        post_data ={
                'username': username,
                'password': password,
                'commit':'Я уже бог!',
                'save_login':'true',
                }
        post_data = urllib.urlencode( post_data )
        req = urllib2.Request( url, post_data, __headers )
        try:
            u = urllib2_30x.open( req)
        except urllib2.HTTPError, e:
            if e.code == 302:
                self.cookie = e.headers.get('Set-Cookie')
                self.headers.update( {'Cookie': self.cookie } )
            else:
                raise Exception('Auth falled')

    def get_main_page(self):
        #url = 'http://www.godville.net/hero'
        __headers = self.headers
        __headers.update({
            'Accept':'text/javascript, text/html, application/xml, text/xml, */*',
            'X-Requested-With':'XMLHttpRequest',
                            'X-Prototype-Version':'1.6.0.3',
                            'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'
                            } )
        url = 'http://www.godville.net/hero/update_details'
        post = urllib.urlencode({'':''})
        req = urllib2.Request(url, post, __headers)
        u = urllib2.urlopen(req)
        self.page = u.read()
        print unicode(self.page)

    def parse_block(self, parser, **attrs):
        block = parser('div',attrs)[0]
        for line in block('div',{'class':'new_line'}):
            print line.findAllNext(attrs={'class':'l_capt'})[0].renderContents()
            print line.findAllNext(attrs={'class':'field_content'})[0].renderContents()


    def parse_page(self):
        with open('hero.html','r') as f:
            self.page = f.read()
        init_parser = html5lib.HTMLParser(tree=html5lib.treebuilders.getTreeBuilder("beautifulsoup"))
        parser = init_parser.parse( self.page, 'utf-8' )
        #print parser.prettify()
        self.parse_block(parser, id='hero_info')
        self.parse_block(parser, id='hero_stats')




class API:
    'http://wiki.godville.net/index.php/API'
    api_url = 'http://www.godville.net/gods/api/%s.json'
    names_keys = {
            u'clan':u'Гильдия',
            u'distance':u'Столбов от столицы',
            u'gold_approx':u'Золотых',
            u'inventory_max_num':'',
            u'inventory_num':u'Инвентарь',
            u'max_health':'',
            u'health':u'Здоровье',
            u'name':u'Имя',
            u'quest_progress':u'Прогресс задания',
            u'quest':u'Задание',
            u'level':u'Уровень',
            u'gender':u'Пол',
            u'godpower':u'Прана',
            u'arena_fight':u'Сражение на арене',
            u'inventory':u'Инвентарь',
            u'diary_last':u'Дневник героя',
            u'godname':u'Бог',
            u'motto':u'Девиз',
            u'town_name':u'Город',
            u'exp_progress':u'Прогресс опыта',
            u'alignment':u'Характер'
            }

    def __init__(self):
        pass
    def load(self, godname='Godville'):
        json_url = self.api_url%godname
        _u = urllib.urlopen( json_url)
        json_src = _u.read()
        if not json_src == 'Not found':
            self.json_loads = json.loads(json_src)
        else:
            raise Exception('Not found godname. Please enter correct name')
    def get_info(self):
        return self.json_loads
    def get_names_keys(self):
        return self.names_keys
    def get_name_key(self, key):
        name = self.names_keys.get(key)
        if name:
            return name
        else:
            return key
    def print_info(self):
        for k,v in self.json_loads.items():
            if k == 'inventory':
                print self.get_name_key( k )
                for ik,iv in v.items():
                    print '* ',ik, ', '.join( [ 
                        ':'.join((str(i[0]),str(i[1])) ) for i in iv.items()
                            ]  )
            else:
                print k, self.get_name_key(k), ': ',v


class CliApp(API):
#http://www.pixelbeat.org/docs/terminal_colours/
    COLOR = {
            'level':'\033[1;33m',
            'health':'\033[31m',
            'items':'\033[33m',
            'quest':'\033[1;35m'
            }
    ENDC = '\033[0m'
    def __init__(self, name):
        self.name = name
        pass
    def corored_text(self, text, color):
        return ''.join( (color, text, self.ENDC ) )
    def progress(self, current, max, segments=30,full='=', none=' ' ):

        current_segments = (current*segments)/max
        return '['+''.join(
                [full for i in xrange( current_segments-1) ]
                )+'|'+''.join(
                [ none for i in xrange( segments-current_segments)]
                )+']'

    def make_out(self):
        __out = u'''Бог:            %(godname)s
Имя:            %(name)s
Пол:            %(gender)s
Девиз:          %(motto)s
Характер:       %(alignment)s
Гильдия:        %(clan)s
Уровень:        %(level_progress)s %(level)i
Инвентарь:      %(inventory_progress)s %(inventory_num)i / %(inventory_max_num)i
Здоровье:       %(health_progress)s %(health)i / %(max_health)i
Задание:        %(quest_progress_bar)s %(quest_progress)s
    %(quest)s
Золотых:        %(gold_approx)s
%(distance)s
Прана:          %(godpower_progress)s %(godpower)i
Дневник героя:  %(diary_last)s

%(inventory)s
        '''
        __info = self.get_info()
        __info.update(
                {
                    'inventory':
                        u'В карманах у героя можно найти:\n%s'%'\n'.join(
                        [ '* '+i for i in __info.get('inventory').keys()]
                            ) if __info.get('inventory') else u'В карманах у героя пусто.',
                    'distance':
                        u'Город: %10s'% __info.get('town_name') if __info.get('town_name') else u'Столбов от столицы: %10i шт'%__info.get('distance'),
                    'level_progress':
                        self.progress(__info.get('exp_progress'),100),
                    'inventory_progress':
                        self.progress(__info.get('inventory_num'),
                            __info.get('inventory_max_num')),
                    'health_progress':
                        self.progress(__info.get( 'health'),
                            __info.get('max_health'),),
                    'godpower_progress':
                        self.progress(__info.get('godpower'),100),
                    'quest_progress_bar':
                        self.progress(__info.get('quest_progress'),100),


                    })

        _o = __out%__info
        self.lines = len(_o.split('\n'))
        return _o
    def clear(self):
        return ''.join( [ '\r' for i in xrange( self.lines )] )


    def main(self, delay=60):
        import time, os
        while True:
            os.system('clear')
            self.load(self.name)
            print self.make_out()
            time.sleep(delay)

        pass

if __name__ == '__main__':
    import os
    name = os.sys.argv[1]
    c = CliApp(name)
    #c.load(name)
    #c.print_info()
    c.main()

    #p =ParsePage()
    #p.auth(name,pass)
    #p.get_main_page()
    #p.parse_page()
