#! /usr/bin/python                                                                            
# -*- coding: utf-8 -*-

# KDE4 plasmoid example
#
# 2009 (c) Andy Mikhailenko <http://neithere.net>
#
# homepage: http://bitbucket.org/neithere/plasma-test/
# based on: http://habrahabr.ru/blogs/python/50184/ by Deepwalker
#
# Please feel free to fork this script and create your own plasmoids.

from PyQt4.QtCore import *
from PyQt4.QtGui import * 
from PyKDE4.kdecore import *
from PyKDE4.kdeui import *  
from PyKDE4.plasma import Plasma
from PyKDE4 import plasmascript

from godvilleclient import API

class TestApplet(plasmascript.Applet):
       
    def __init__(self, parent, args=None):        
        plasmascript.Applet.__init__(self, parent)

    def init(self):
        self.setHasConfigurationInterface(False)
        
        self.setAspectRatioMode(Plasma.IgnoreAspectRatio)
        self.mlayout = QGraphicsLinearLayout(Qt.Horizontal)

        self.label = Plasma.Label()
        self.label.setText("Test 123...")   #open('/proc/loadavg','r').read().strip())
        self.mlayout.addItem(self.label)
        
        self.setLayout(self.mlayout)

    def contextualActions(self):
        return []

    def paintInterface(self, painter, option, rect):
        pass

    def constraintsEvent(self, constraints):
        if constraints & Plasma.SizeConstraint:
            self.resize(self.size())

    def mousePressEvent(self, event):
        pass

    def dragEnterEvent(self, e):
        e.accept()

    def dropEvent(self, e):
        QMessageBox.warning(None, "Drop Event","Got this event: %s" % e)

def CreateApplet(parent):
     return TestApplet(parent)

