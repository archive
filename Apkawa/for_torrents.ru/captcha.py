import pygtk
pygtk.require('2.0')

import gtk
import gtk.glade
import gobject
import urllib, Image, StringIO
#import tempfile

class Captcha:
    def __init__(self):
        _xml = 'captcha.glade'
        _window = 'Captcha'
        self.widgets_tree = gtk.glade.XML( _xml)
        events_connect = {
            'enter_captcha_clicked_cb':self.return_val ,
            'text_captcha_activate_cb':self.return_val ,
        }
        self.widgets_tree.signal_autoconnect( events_connect )
        window = self.widgets_tree.get_widget( _window)
        if (window):
            window.connect("destroy", gtk.main_quit)
        self.captcha  = self.widgets_tree.get_widget( 'captcha' )
        self.text_captcha  = self.widgets_tree.get_widget( 'text_captcha' )
    def get_img(self, url):
        src_img = StringIO.StringIO( urllib.urlopen(url).read() )
        img = Image.open(src_img)
#file1 = StringIO.StringIO()
        img.save( 'captcha_img', img.format)
        self.captcha.set_from_file('captcha_img')
        '''
        contents = file1.getvalue()
        file1.close()
        loader = gtk.gdk.PixbufLoader('pnm')
        loader.write( contents, len(contents) )
        pixbuf = loader.get_pixbuf()
        loader.close()

        self.captcha.set_from_pixbuf(pixbuf)
        '''
    def return_val(self, widget):
#print widget.name
        print self.text_captcha.get_text()
        gtk.main_quit()
    




if __name__ == "__main__":
    import sys
    app = Captcha()
#app.get_img('http://spamavert.com/images/logo.spamavert.com.jpg')
    app.get_img(sys.argv[1])
    gtk.main()
    '''
 import StringIO
 import PIL.Image
 import gtk

 def image_to_pixbuf(image):
     fd = StringIO.StringIO()
     image.save(fd, "ppm")
     contents = fd.getvalue()
     fd.close()
     loader = gtk.gdk.PixbufLoader("pnm")
     loader.write(contents, len(contents))
     pixbuf = loader.get_pixbuf()
     loader.close()
     return pixbuf

 image = PIL.Image.open("example.png")
 pixbuf = image_to_pixbuf(image)
 image = gtk.Image()
 image.set_from_pixbuf(pixbuf)

    '''

