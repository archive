# -*- coding: utf-8 -*-
import re #,pyparse


class AnimeRename:
    VIDEO_TYPE =  ('avi','mkv','mp4','ogm', )
    SUBTITLE_TYPE = ('ass','srt','ssa',)

    re_delim = re.compile('(?!\!)[\W_]')
    re_chapter = re.compile('^(?:ep|_|)(\d{1,3})(?:v\d|)$')

    re_hash = re.compile('[A-Z0-9]{8}')

    def __init__(self, files):
        self.videos = [ self._parse( v ) for v in files if v.endswith( self.VIDEO_TYPE ) ]
        self.subs = [ self._parse( s )  for s in files if s.endswith( self.SUBTITLE_TYPE )]

    def analize( self):
        for video in self.videos:
            video['subs']  = {}
            for sub in self.subs:
                if sub['chapter'] == video['chapter']:
                    #_temp_sub = { 1 : sub['filename'] } 
                    ratio = 0
                    for sub_p in sub['pieces']:
                        if sub_p in video['pieces']:
                            ratio += 1
                    video['subs'].update( { ratio : sub['filename'] } )
            video['sub'] = video['subs'][max(video['subs'].keys())]
            print video
        


    def _parse( self, filename ):
        chapter= None
        pieces = self.re_delim.split( filename )
        for piece in pieces:
            if self.re_chapter.match(piece):
                chapter = int(self.re_chapter.findall( piece )[0])
        return { 'filename':filename, 'chapter':chapter, 'pieces':pieces}


if __name__ == '__main__':
    TEST = '''Examples:
Aim_For_The_Top_2!_Diebuster-ep1v3.DVD(H264.AC3)[KAA][48ECC48D].mkv
Aim For The Top 2! Diebuster - 1.srt
Aim_For_The_Top_2!_Diebuster-ep01.DVD(H264.AC3)[KAA][E41C5177].srt
Aim_For_The_Top_2!_Diebuster-ep2.DVD(H264.AC3)[KAA][E41C5177].mkv
Aim_For_The_Top_2!_Diebuster-ep2.DVD(H264.AC3)[KAA][E41C5177].srt
Aim_For_The_Top_2!_Diebuster-ep3.DVD(H264.AAC)[KAA][8E604446].mkv
Aim_For_The_Top_2!_Diebuster-ep3.DVD(H264.AAC)[KAA][8E604446].srt
Aim_For_The_Top_2!_Diebuster-ep4.DVD(H264.AAC)[KAA][A8AF55D6].mkv
Aim_For_The_Top_2!_Diebuster-ep4.DVD(H264.AAC)[KAA][A8AF55D6].srt
Aim_For_The_Top_2!_Diebuster-ep5.DVD(H264.AAC)[KAA][F6276C63].mkv
Aim_For_The_Top_2!_Diebuster-ep5.DVD(H264.AAC)[KAA][F6276C63].srt
Aim_For_The_Top_2!_Diebuster-ep6.DVD(H264.AAC)[KAA][52325748].mkv
Aim_For_The_Top_2!_Diebuster-ep6.DVD(H264.AAC)[KAA][52325748].srt

[SOSG][Minami-Ke_Okaeri][01][X264_AAC][1280x720].ass
[SOSG][Minami-Ke_Okaeri][01][X264_AAC][1280x720].mkv
[SOSG][Minami-Ke_Okaeri][02][X264_AAC][1280x720].ass
[SOSG][Minami-Ke_Okaeri][02][X264_AAC][1280x720].mkv
[SOSG][Minami-Ke_Okaeri][03][X264_AAC][1280x720].ass
[SOSG][Minami-Ke_Okaeri][03][X264_AAC][1280x720].mkv

xxxHOLiC Kei [Zero-Raws] - 01.ass
xxxHOLiC Kei [Zero-Raws] - 01.mp4
xxxHOLiC Kei [Zero-Raws] - 02.ass
xxxHOLiC Kei [Zero-Raws] - 02.mp4
xxxHOLiC Kei [Zero-Raws] - 03.ass
xxxHOLiC Kei [Zero-Raws] - 03.mp4
'''
    ar=AnimeRename( TEST.split('\n') )
    ar.analize()




    
