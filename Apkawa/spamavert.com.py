#!/usr/bin/python
# -*- coding: utf-8 -*-
###
#This file is part of <name prog> project
#
#<описание программы>
#Copyright (C) <year> <name|nick>
#
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#You can contact author by email <my email>
###

import urllib
from xml.dom import minidom
from tempfile import _RandomNameSequence as RandName

class Spamavert:
    host = 'http://spamavert.com'
    feed = '/feed.php?alias='
    def __init__(self, suffix = '', prefix=''):
        self.name = suffix+RandName().next()+prefix
        self.url = self.host+self.feed+self.name
    def get_email(self):
        return self.name+'@spamavert.com'
    def get_post(self, s=0):

        if s:
            import time
            sleep = lambda s: time.sleep(s)
        else:
            sleep = lambda s: s
            #sleep(sleep)
        for i in xrange(100):
            sleep(s)
            xml_src = urllib.urlopen( self.url ).read()
            xml_dom = minidom.parseString(xml_src)
            items = xml_dom.getElementsByTagName('item')
            if items:
                item = items[0]
                post = {
                        'title' :item.getElementsByTagName('title')[0].firstChild.data,
                        'link': item.getElementsByTagName('link')[0].firstChild.data,
                        'description' :item.getElementsByTagName('description')[0].firstChild.data,
                        'date':item.getElementsByTagName('dc:date')[0].firstChild.data,
                        }
                return post
            else:
                continue

if __name__ == '__main__':
    spam = Spamavert()
    print spam.get_email()

    print spam.get_post(10)


