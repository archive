#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* 
 * Apkawa <apkawa@gmail.com>
 * 
 */
#define IN_FILE_PATH "task_15.in"
#define OUT_FILE_PATH "task_15_2.out"
#define MAX_LINES 10
#define MAX_LENGTH_LINE 100
#define FAMILY_LENGTH 15
#define PHONE_LENGTH 9
#define family 0
#define phone 1
int main ( int argc, char *argv[] )
{
    FILE *in_file;
    FILE *out_file;
    int i,j,t;
    char line[MAX_LENGTH_LINE];
    char lines[MAX_LINES][2][24];
    char *result[2];


    in_file = fopen( IN_FILE_PATH, "r");
    for (i = 0; i < MAX_LINES; i++) {
        fgets( line, MAX_LENGTH_LINE ,in_file);
        for (j = 0; j < FAMILY_LENGTH; j++) {
            lines[i][family][j] = line[j];
        }
        lines[i][family][FAMILY_LENGTH] = '\0';

        for (j = 0; j < PHONE_LENGTH; j++) {
            lines[i][phone][j] = line[FAMILY_LENGTH+j];
        }
        lines[i][phone][PHONE_LENGTH] = '\0';
        printf("%i %s|%s|\n", i, lines[i][family], lines[i][phone]);
        if (i != 0) {
            for ( t=0; t < 2; t++){
                for ( j=0; j <= strlen( lines[i][t] ); j++) {
                    if ( lines[i][t][j] != *(result[t]+t) ){
                        if ( lines[i][t][j] < *(result[t]+t) ){
                            result[t] = *lines[i];
                        } else {
                            break;  
                        }  
                    }
                }
            }
        } else {
            result[0] = *lines[i];
            result[1] = *lines[i];
        }
    }
    printf("%i %i\n", result[0], result[1]);
    printf("min family \n%s\n", &result[family][0]) ;
    printf("min phone \n%s\n",  &result[phone][0] );

    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
