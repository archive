program lab4;
type
    uelem_type = ^element_type;
    element_type = record
                    a: integer;
                    n: integer;
                    next: uelem_type;
                end;

function pow( a,b:real):real;
{ функция возведения в степень (a^b) }
var t:real;
begin
     t := abs(a);
     if a < 0 then pow := (-1)*exp(b*ln(t))
              else pow := exp(b*ln(t));
end;

    
function parse_polinom(str_line:string): uelem_type;
    var
        polinom, polinom_first: uelem_type;
        i, p, code: integer;
    begin
        new(polinom_first);
        polinom := polinom_first;
        p:=1;
        for i:=1 to ord( str_line[0] ) do
            begin
                if str_line[i] <> ' ' then
                    begin
                        with polinom^ do
                            begin
                            case p of
                                1:
                                    begin
                                        val( str_line[i], a, code);
                                        p := 2;
                                    end;
                                2:
                                    begin
                                        val( str_line[i], n, code);
                                        new( next);
                                        polinom := next;
                                        p := 1;
                                    end;
                                end;
                            end;
                    end;
            end;
        polinom^.next := nil;
        parse_polinom := polinom_first;
            
    end;
function calc_polinom( x: longint; polinom: uelem_type):longint;
    var
        res: longint;
    begin
        res := 0;
        while True do
            begin
                with polinom^ do
                    begin
                        if next = nil then
                            break;
                        res := res +( a* round(pow(x,n) ));

                        writeln( a, ' ', n, ' ',res );
                        polinom := next
                    end;
            end;
        calc_polinom:= res;
    end;

function main: integer;
    const
        IN_PATH = 'lab4.in';
        OUT_PATH = 'lab4.out';
    var
        in_file: text;
        out_file: text;
        str_line: string;
    begin
        assign( in_file, IN_PATH);
        reset( in_file);
        readln( in_file, str_line);
        close( in_file);
        
        assign(out_file, OUT_PATH);
        rewrite( out_file);
        writeln(out_file, str_line);
        writeln(out_file,'----');
        writeln( out_file,  calc_polinom( 69, parse_polinom( str_line ) ) );
        close(out_file);


    end;

begin 
    main;
end.
