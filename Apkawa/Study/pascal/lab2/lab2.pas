program lab2;
{ centred line  use linear list}
{ tips for fpc - "fpc -mTP name_prog.pas" } 
{ write code in C99 style}
{   
    author:  Apkawa
    email:   apkawa@gmail.com
    license: GPLv3
}
procedure centred(  width_screen: integer; var line_str: string);
    var
        o,offset: integer;
        length_str: integer;
        left_fulled: string;

    begin
        length_str:= ord( line_str[0]);
        offset := round( (width_screen - length_str)/2 );
        {alternative: left_fulled:= ' '*offset}
        left_fulled := ' ';
        for o:=1 to offset do
            left_fulled := left_fulled + ' ';
        line_str := concat( left_fulled, line_str);
    end;

procedure cut( var line_str: string);
    var
        i,start_pos, end_pos: integer;
        start_fix_flag, end_fix_flag: boolean;
    begin
        start_fix_flag := False;
        end_fix_flag := False;
        start_pos := 1;
        end_pos := ord(line_str[0]);
        for i:=1 to ord(line_str[0]) do
            begin
                if not start_fix_flag then
                    begin
                        if line_str[start_pos] = ' ' then
                            inc(start_pos) {end_pos++}
                        else
                            start_fix_flag := True;
                    end;
                if not end_fix_flag then
                    begin
                        if line_str[end_pos] = ' ' then
                            dec(end_pos){ end_pos-- }
                        else
                            end_fix_flag := True;
                    end
            end;
        line_str := copy( line_str, start_pos, (end_pos+1)-start_pos);
    end;

function main : integer;
    const
        IN_FILE_PATH = 'lab2.in';
        OUT_FILE_PATH = 'lab2.out';
        SCREEN_WIDTH = 80;
        MAX_LENGTH_LINE = 40;
    type
        uline_type = ^line_type;
        line_type = record
                        str_line: string;
                        next: uline_type;
                    end;
    var
        uline_first, uline_current, uline_next : uline_type;
        in_file, out_file: text;
    begin
        assign( in_file, IN_FILE_PATH);
        reset( in_file);
        assign( out_file, OUT_FILE_PATH);
        rewrite( out_file);

        new( uline_first);
        uline_current := uline_first;

        while True do
            begin
                with uline_current^ do
                    begin
                    readln( in_file, str_line);
                    writeln( out_file, str_line);

                    if eof( in_file) then
                        begin
                            next := nil;
                            close( in_file);
                            break;
                        end
                    else
                        new( uline_next);
                        next := uline_next;

                    end;
                uline_current := uline_next;
            end;

        writeln( out_file);
        writeln( out_file, 'Result');
        uline_current := uline_first;
        while True do
            begin
                with uline_current^ do
                    begin
                        cut( str_line);
                        centred( SCREEN_WIDTH, str_line);
                        writeln( out_file, str_line);
                        if not (next = nil) then
                            uline_current := next
                        else
                            break
                    end;
            end;


        close( out_file);
        main := 1;
    end;
begin
    main();
end.
