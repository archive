program lab3;
{ tips for fpc - "fpc -mTP name_prog.pas" } 
{ write code in C99 style}
{   
    author:  Apkawa
    email:   apkawa@gmail.com
    license: GPLv3
}
type
    uline_type = ^list_type;
    list_type  = record
                    str_word: string[20];
                    link: array [1..2] of uline_type;
                    {1: original position, 2: sorted by alpha position}
                end;
	link_to_first_type = array [1..2] of uline_type;
var
	ufirst: link_to_first_type;

procedure P1( in_path: string; var uline_first: link_to_first_type);
    var
        in_file: text;
		uline_next: uline_type;
        uline_current, uline_alpha, uline_alpha_parent: uline_type;
		AA: uline_type;
		i: integer;
    begin
        assign( in_file, in_path);
        reset( in_file);
		new( uline_first[1]);
		uline_current := uline_first[1];
		new(AA);
		while True do
			begin
				with uline_current^ do
					begin
						link[2] := AA;
						if not eof( in_file) then
							begin
								readln( in_file, str_word);
								new( uline_next);
								link[1] := uline_next;
								uline_current := uline_next;
							end
						else 
							begin
								link[1] := nil;
								break;
							end;

					end;
			end;
		close( in_file);
		{ get first head}
		uline_current := uline_first[1];
		uline_alpha := uline_first[1];
		uline_alpha_parent:= nil;
		uline_first[2] := nil;
{FAIL}
i := 0;
		while True do
			begin
                while True do
                    begin
                        if uline_current^.link[1] = nil then
                            break;
                        if uline_current^.link[2] = AA then
                            begin
                                inc(i);
                                if uline_alpha = nil then
                                    begin
                                        uline_alpha := uline_current;
                                    end;
                                if uline_current^.str_word < uline_alpha^.str_word  then
                                    begin
                                        uline_alpha := uline_current;
                                    end;
                            end;
                                
                        uline_current := uline_current^.link[1];
                    end;
                if i = 0 then
                    begin
                        uline_alpha_parent^.link[2] := nil;
                        break;
                    end;

                if uline_first[2] = nil then
                    begin
                        uline_alpha_parent := uline_alpha;
                        uline_first[2]:= uline_alpha;
                    end
                else
                    begin
                        uline_alpha_parent^.link[2] := uline_alpha;
                        uline_alpha_parent := uline_alpha;
                    end;
                i:=0;
                uline_current := uline_first[1];
                uline_alpha := nil;
            end;

    end;{END PROCEDURE P1 }
procedure P2( out_path: string; key: integer; var uline_first: link_to_first_type );
	var
		out_file: text;
        uline_current : uline_type;
	begin
	assign( out_file, out_path);
	rewrite( out_file);
	uline_current := uline_first[key];
	while True do
		begin
		writeln( uline_current^.str_word );
		writeln( out_file, uline_current^.str_word );
		uline_current:= uline_current^.link[key];
		if uline_current^.link[key] = nil then
			break;
		end;
    close( out_file);
	end;
function main:integer;
    const
        IN_FILE_PATH = 'lab3.in';
        OUT_FILE_PATH = 'lab3.out';
    begin
	P1( IN_FILE_PATH, ufirst);
	P2( OUT_FILE_PATH, 1, ufirst);
main := 1;
    end;
begin
    main;
end.
