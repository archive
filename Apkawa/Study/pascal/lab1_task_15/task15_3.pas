program task_15_mod_2;
{ use record type and use type file }
{ tips for fpc - "fpc -mTP name_prog.pas" } 
const
    IN_FILE_PATH    = 'task_15.in';
    IN_FILE_TYPESED_PATH = 'task15_3.tin';
    OUT_FILE_PATH   = 'task15_3.out';
    MAX_LENGTH_LINE = 24;
    MAX_LINES       = 10;
    START_FAMILY    = 1;
    END_FAMILY      = 15;
    START_PHONE     = 16;
    END_PHONE       = MAX_LENGTH_LINE;

type
    family = string[15];
    phone = longint;
    type_line = record
                    family: family;
                    phone: phone;
                end;
    array_type_line = array [1..MAX_LINES] of type_line;
    result_line_type = type_line;

var
    in_file_typesed: file of type_line; {typesed file}
    in_file, out_file: text;
    i:integer;
    in_line: type_line;
    family_line, phone_line: result_line_type;

procedure convert_text_to_typesed_file;
    begin
        assign( in_file, IN_FILE_PATH);
        reset( in_file);
        assign( in_file_typesed, IN_FILE_TYPESED_PATH);
        rewrite( in_file_typesed);

        for i:=1 to MAX_LINES do
            begin
                {START read from in_file and write in out_file}
                readln( in_file, in_line.family, in_line.phone );
                write( in_file_typesed, in_line );
                {END}
            end;

        close( in_file_typesed);
        close(in_file);
    end;


begin
    convert_text_to_typesed_file; { <- create typesed file}
    {begin work on typesed file}
    assign( out_file, OUT_FILE_PATH);
    rewrite( out_file);
    assign( in_file_typesed, IN_FILE_TYPESED_PATH);
    reset( in_file_typesed);

    for i:=1 to filesize( in_file_typesed ) do
        begin
            read( in_file_typesed, in_line );
            with in_line do
                begin
                    writeln( out_file, family, phone);
                    {find min family and phone}

                    if i > 1 then
                        begin
                            if family_line.family > family then
                                family_line := in_line;
                            if phone_line.phone > phone then 
                                phone_line := in_line;
                        end
                    else
                        begin
                        family_line := in_line;
                        phone_line := in_line;
                        end;
                end;
        end;

    writeln(out_file);
    writeln(out_file,'Result');
	writeln( out_file, 'min family');
    with family_line do
        writeln( out_file, family,phone);
	writeln( out_file, 'min phone');
    with phone_line do
        writeln( out_file, family,phone);
    close( out_file);
end.
