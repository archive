program task_15_mod_6;
{ use task_15_2 and recursion }
{ tips for fpc - "fpc -mTP name_prog.pas" } 
const
    IN_FILE_PATH    = 'task_15.in';
    OUT_FILE_PATH   = 'task15_6.out';
    MAX_LENGTH_LINE = 24;
    MAX_LINES       = 10;
    START_FAMILY    = 1;
    END_FAMILY      = 15;
    START_PHONE     = 16;
    END_PHONE       = MAX_LENGTH_LINE;
type
    result_line_type = string[MAX_LENGTH_LINE];
    in_lines_type = array [1..MAX_LINES] of result_line_type;
var
    in_file, out_file: text;
    i:integer;
    in_lines: in_lines_type;
    family_line, phone_line: result_line_type;

{ search minimal family or phone }
procedure find_min( i, START_PIECE, END_PIECE: integer; var result_line: result_line_type);
    var
        in_t : string;
        res_t : string;

    begin
        if i > 1 then
            begin
            in_t := copy( in_lines[i], START_PIECE, END_PIECE);
            res_t := copy( result_line, START_PIECE, END_PIECE);
            if in_t < res_t then
                result_line := in_lines[i];
            end
        else
            result_line := in_lines[i];
    end;

procedure read_file(i: integer);    {recursion procedure}
	begin
		if i <= MAX_LINES then
			begin
				{START read from in_file and write in out_file}
				readln( in_file, in_lines[i]);
				writeln( out_file, in_lines[i]);
				{END}
				{call search procedure}
				find_min( i, START_FAMILY, END_FAMILY, family_line );
				find_min( i, START_PHONE, END_PHONE, phone_line );
				{ self call. This recursion }
				read_file( i+1);
			end
	end;
    

begin
    assign( in_file, IN_FILE_PATH);
    reset( in_file);
    assign( out_file, OUT_FILE_PATH);
    rewrite( out_file);

    read_file(1); { call recursion procedure }

    close(in_file);
    writeln( out_file);
    writeln( out_file,'Result');
    writeln( out_file, 'min family');
    writeln( out_file, family_line);
    writeln( out_file, 'min phone');
    writeln( out_file, phone_line);
    close( out_file);
end.
