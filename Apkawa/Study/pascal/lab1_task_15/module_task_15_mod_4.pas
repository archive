unit module_task_15_mod_4;

interface
    const
        MAX_LENGTH_LINE = 24;
        MAX_LINES       = 10;
        START_FAMILY    = 1;
        END_FAMILY      = 15;
        START_PHONE     = 16;
        END_PHONE       = MAX_LENGTH_LINE;
    type
        result_line_type = string[MAX_LENGTH_LINE];
        in_lines_type = array [1..MAX_LINES] of string[MAX_LENGTH_LINE];
    var
        in_lines: in_lines_type;
        result_line: result_line_type;
        
    procedure parse_file( IN_FILE_PATH, OUT_FILE_PATH:string );
    procedure find_min_phone_and_first_letter_family( i: integer; var result_line: result_line_type);

implementation
    procedure parse_file;
        var
            in_file, out_file: text;
            i:integer;
        begin
            assign( in_file, IN_FILE_PATH);
            reset( in_file);
            assign( out_file, OUT_FILE_PATH);
            rewrite( out_file);

            for i:=1 to MAX_LINES do
                begin
                    {START read from in_file and write in out_file}
                    readln( in_file, in_lines[i]); 
                    writeln( out_file, in_lines[i]);
                    {END}
                    find_min_phone_and_first_letter_family( i, result_line );
                end;
            close(in_file);
            writeln(out_file);
            writeln(out_file,'Result');
            writeln(out_file,result_line);
            close( out_file);
        end;
    procedure find_min_phone_and_first_letter_family;
            var
                t: integer;
            begin
                if i > 1 then
                    begin
                    if in_lines[i,START_FAMILY] <=  result_line[START_FAMILY]  then
                        begin
                        for t:= START_PHONE to END_PHONE do
                            begin
                            if in_lines[i,t] <> result_line[t] then
                                begin
                                if in_lines[i,t] < result_line[t] then
                                    begin
                                    result_line := in_lines[i];
                                    end;
                                break;
                                end;
                            end;
                        end;
                    end
                else
                    result_line := in_lines[i];
            end;
            {END procedure find_min_phone_and_first_letter_family}
