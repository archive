program task_15_mod_2;
{ use record type and use type file }
{ tips for fpc - "fpc -mTP name_prog.pas" } 
const
    IN_FILE_PATH    = 'task_15.in';
    IN_FILE_TYPESED_PATH = 'task_15.intypesed';
    OUT_FILE_PATH   = 'task_15_mod_3.out';
    MAX_LENGTH_LINE = 24;
    MAX_LINES       = 10;
    START_FAMILY    = 1;
    END_FAMILY      = 15;
    START_PHONE     = 16;
    END_PHONE       = MAX_LENGTH_LINE;
type
    family = string[15];
    phone = longint;
    type_line = record
                        family: family;
                        phone: phone;
                    end;

var
    in_file_typesed: file of type_line;
    in_file, out_file: text;
    i:integer;
    in_lines: array [1 .. MAX_LINES] of type_line;
    result_line: type_line;

procedure find_min_phone_and_first_letter_family(i: integer; var result_line: type_line );
        begin
            if i >1 then
                begin
                if in_lines[i].family[1]  <=  result_line.family[1]  then
                    begin
                        if in_lines[i].phone < result_line.phone then
                            begin
                            result_line := in_lines[i];
                            end;
                    end;
                end
            else
                result_line := in_lines[i];
        end;
        {END procedure find_min_phone_and_first_letter_family}
    procedure convert_text_to_typesed_file;
        begin
            assign( in_file, IN_FILE_PATH);
            reset( in_file);
            assign( in_file_typesed, IN_FILE_TYPESED_PATH);
            rewrite( in_file_typesed);

            for i:=1 to MAX_LINES do
                begin
                    {START read from in_file and write in out_file}
                    readln( in_file, in_lines[i].family, in_lines[i].phone );
                    write( in_file_typesed, in_lines[i] );
                    {END}
                end;
            close( in_file_typesed);
            close(in_file);
        end;
begin
    convert_text_to_typesed_file;
    assign( out_file, OUT_FILE_PATH);
    rewrite( out_file);
    assign( in_file_typesed, IN_FILE_TYPESED_PATH);
    reset( in_file_typesed);

    for i:=1 to MAX_LENGTH_LINE do
        begin
            read( in_file_typesed, in_lines[i] );
            find_min_phone_and_first_letter_family( i, result_line);
            writeln( out_file, in_file[i].]family, phone);
        end;

    writeln(out_file);
    writeln(out_file,'Result');
    with  result_line do
        writeln( out_file, family, phone);
    close( out_file);
