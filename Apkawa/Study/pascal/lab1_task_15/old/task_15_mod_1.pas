program task_15_mod_1;
{ use two-dimensional array and not use procedure }
{ tips for fpc - "fpc -mTP name_prog.pas" } 
const
    IN_FILE_PATH    = 'task_15.in';
    OUT_FILE_PATH   = 'task_15_mod_1.out';
    MAX_LENGTH_LINE = 24;
    MAX_LINES       = 10;
    START_FAMILY    = 1;
    END_FAMILY      = 15;
    START_PHONE     = 16;
    END_PHONE       = MAX_LENGTH_LINE;
var
    in_file, out_file: text;
    i,j, t:integer;
    in_lines: array [1..MAX_LINES,1..MAX_LENGTH_LINE] of char;
    result_line: string[MAX_LENGTH_LINE];
begin
    assign( in_file, IN_FILE_PATH);
    reset( in_file);
    assign( out_file, OUT_FILE_PATH);
    rewrite( out_file);

    for i:=1 to MAX_LINES do
        begin
            {START read from in_file and write in out_file}
            for j:=1 to MAX_LENGTH_LINE do
                begin
                read( in_file, in_lines[i,j]);
                write( out_file, in_lines[i,j]);
                end;
            readln( in_file);
            writeln( out_file);
            {END}
            
            {START FIND TASK}
            if i > 1 then
                begin
                if in_lines[i,START_FAMILY] <=  result_line[START_FAMILY]  then
                    begin
                    for t:=START_PHONE to END_PHONE do
                        begin
                        if in_lines[i,t] <> result_line[t] then
                            begin
                            if in_lines[i,t] < result_line[t] then
                                begin
                                result_line := in_lines[i];
                                end;
                            break;
                            end;
                        end;
                    end;
                end
            else
                result_line := in_lines[i];
            {END FIND TASK}
        end;
    close(in_file);
    writeln(out_file);
    writeln(out_file,'Result');
    writeln(out_file,result_line);
    close( out_file);
end.
