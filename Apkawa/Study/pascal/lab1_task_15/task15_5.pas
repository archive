program task_15_mod_5;
{ use linear structure }
{ tips for fpc - "fpc -mTP name_prog.pas" } 
const
    IN_FILE_PATH    = 'task_15.in';
    OUT_FILE_PATH   = 'task15_5.out';
    MAX_LENGTH_LINE = 24;
    MAX_LINES       = 10;
    START_FAMILY    = 1;
    END_FAMILY      = 15;
    START_PHONE     = 16;
    END_PHONE       = MAX_LENGTH_LINE;

procedure main( in_path, out_path: string);
    type
        uline_type = ^type_line ;
        type_line = record
                        family: string[15];
                        phone: longint;
                        next_line : uline_type;
                    end;

    var
        in_file, out_file: text;
        i:integer;
        uline_cur, uline_next: uline_type;
        family_line, phone_line: uline_type;
    begin
        assign( in_file, in_path);
        reset( in_file);

        assign( out_file, out_path);
        rewrite( out_file);
        
        new( uline_cur );
        for i:=1 to MAX_LINES do
            begin
                with uline_cur^ do
                    begin
                        readln( in_file, family, phone );
                        writeln( family, phone);
                        new( uline_next);
                        next_line := uline_next;

                        writeln( out_file, family, phone);
                        {find min family and phone}

                        if i > 1 then
                            begin
                                if family_line^.family > family then
                                    family_line := uline_cur;
                                if phone_line^.phone > phone then 
                                    phone_line := uline_cur;
                            end
                        else
                            begin
                                family_line := uline_cur;
                                phone_line := uline_cur;
                            end;
                    end;
                uline_cur := uline_next;
            end;

        writeln(out_file);
        writeln(out_file,'Result');
        writeln( out_file, 'min family');
        with family_line^ do
            writeln( out_file, family,phone);
        writeln( out_file, 'min phone');
        with phone_line^ do
            writeln( out_file, family,phone);
        close( out_file );
        close( in_file );
    end;
begin
    main( IN_FILE_PATH, OUT_FILE_PATH);
end.
