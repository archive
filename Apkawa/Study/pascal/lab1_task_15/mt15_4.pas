unit mt15_4;

interface
    type
        result_line_type = string;
        
    procedure find_min( in_line: string; i,START_PIECE, END_PIECE: integer; var result_line: result_line_type);

implementation
    procedure find_min;
        var
            in_t : string;
            res_t : string;

        begin
            if i > 1 then
                begin
                in_t := copy( in_line, START_PIECE, END_PIECE);
                res_t := copy( result_line, START_PIECE, END_PIECE);
                if in_t < res_t then
                    result_line := in_line;
                end
            else
                result_line := in_line;

        end;
end.
