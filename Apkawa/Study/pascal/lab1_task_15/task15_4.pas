program task_15_mod_4;
uses mt15_4 { use module mt15_4 } ;
{ use mod 2 with module }
{ tips for fpc - "fpc -mTP name_prog.pas" } 
const
    IN_FILE_PATH    = 'task_15.in';
    OUT_FILE_PATH   = 'task15_4.out';
    MAX_LENGTH_LINE = 24;
    MAX_LINES       = 10;
    START_FAMILY    = 1;
    END_FAMILY      = 15;
    START_PHONE     = 16;
    END_PHONE       = MAX_LENGTH_LINE;
type
    in_lines_type = array [1..MAX_LINES] of result_line_type;
var
    in_file, out_file: text;
    i:integer;
    in_lines: in_lines_type;
    family_line, phone_line: result_line_type;
begin
    assign( in_file, IN_FILE_PATH);
    reset( in_file);
    assign( out_file, OUT_FILE_PATH);
    rewrite( out_file);

    for i:=1 to MAX_LINES do
        begin
            {START read from in_file and write in out_file}
            readln( in_file, in_lines[i]);
            writeln( out_file, in_lines[i]);
            {END}
            find_min( in_lines[i], i, START_FAMILY, END_FAMILY, family_line );
            find_min( in_lines[i], i, START_PHONE, END_PHONE, phone_line );
            
        end;
    close(in_file);
    writeln( out_file);
    writeln( out_file,'Result');
    writeln( out_file, 'min family');
    writeln( out_file, family_line);
    writeln( out_file, 'min phone');
    writeln( out_file, phone_line);
    close( out_file);
end.
