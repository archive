program lab5;
type
    utree_type = ^eltree_type;
    eltree_type = record
                    ch: char;
                    left, right: utree_type;
                end;
    stack_type = array [1..255] of real;
const
    OPERATION = ['*','/','-','+'];
    NUM = ['1','2','3','4','5','6','7','8','9','0'];
    OPEN_BRACKET = ['('];
    CLOSE_BRACKET = [')'];
                    
function P( ch: char):integer;
    {Функция приоритета}
    begin
        case ch of
            '(',')','e': P := 0;
            '+','-' : P:= 1;
            '*','/' : P:= 2;
            end;
        
    end;
function infix2postfix( str_line: string):string;
    var 
        i,j:integer; 
        c: integer;
        stack: array [1..255] of char;
        res : string;
        ch : char;
    begin
        i := 1;
        stack[1] := 'e';
        res := '';
        for c:=1 to ord(str_line[0]) do
            begin
                ch := str_line[c];
                if ch in OPERATION then
                    begin
                        while True do
                            begin
                                if P( ch ) > P( stack[i]) then
                                    begin
                                        inc(i);
                                        stack[i] := ch;
                                        break;
                                    end
                                else
                                    begin
                                        res:= res+stack[i];
                                        dec(i);
                                    end;
                            end;{END WHILE}
                    continue;
                    end;{END OPERATION}

                if ch in NUM then
                    begin
                        res:= res+ch;
                    continue;
                    end;

                if ch in OPEN_BRACKET then
                    begin
                        inc(i);
                        stack[i] := ch;
                    continue;
                    end;

                if ch in CLOSE_BRACKET then
                    begin
                        while True do
                            begin
                                if stack[i] in OPEN_BRACKET then
                                    begin
                                        dec(i);
                                        break;
                                    end
                                else
                                    begin
                                        res:= res+stack[i];
                                        dec(i);
                                    end;
                            end;
                    continue;
                    end;{END CLOSE_BRACKET}
            end;{END FOR}

        for j := 1 to i-1 do
            begin
                res := res+stack[i];
            end;
        infix2postfix:= res;
    end;{END infix2postfix}
procedure postfix_tree( str_line: string; var root_tree: utree_type);
    type
        ustack_type = ^elsack_type;
        elsack_type = record
                        tree: utree_type;
                        next: ustack_type;
                      end;
    var
        stack, tmp_stack: ustack_type;
        new_tree: utree_type;
        i,k: integer;
        temp_char:char;
    begin
        stack := nil;
        k := 0;
        for i:= 1 to ord(str_line[0]) do
            begin
                temp_char := str_line[i];
                new(new_tree);
                with new_tree^ do
                    begin
                        left:=nil;
                        right:=nil;
                        ch:=temp_char;

                    end;
                write( new_tree^.ch);
                if temp_char in OPERATION  then
                    begin
                        dec(k);
                        with new_tree^ do
                            begin
                                right := stack^.tree;
                                stack := stack^.next;
                                left := stack^.tree;
                                stack := stack^.next;
                            end;
                    end;
                new( tmp_stack );
                tmp_stack^.next:= stack;
                tmp_stack^.tree:= new_tree;
                stack:= tmp_stack;
                inc(k);
            end;
        root_tree := stack^.tree;
        writeln;
    end;{END postfix_tree}
procedure postfix_tree_recursion( str_line: string; var c:integer; var tree: utree_type);
    begin
        c := c - 1;
        if c > 0 then
            begin
                new(tree);
                tree^.ch := str_line[c];
                if str_line[c] in OPERATION then
                    begin
                        postfix_tree_recursion( str_line, c, tree^.right);
                        postfix_tree_recursion( str_line, c, tree^.left);
                    end
                else
                    begin
                        tree^.right := nil;
                        tree^.left := nil;
                    end;
            end;
    end; {END postfix_tree_recursion}

procedure read_tree( tree: utree_type; var res:string);
    begin{Левое-правое-корень}
        if tree^.left <>nil then 
            read_tree( tree^.left, res);
        if tree^.right <> nil then
            read_tree( tree^.right, res);

        if tree <> nil then 
            res := res + tree^.ch;

    end;{END read_tree}
    
function P1( str_line: string): string;
    var
        root_tree: utree_type;
        c: integer;
        res: string;
    begin
        str_line := infix2postfix( str_line);
        c:= ord(str_line[0])+1;
        postfix_tree_recursion( str_line,c , root_tree);  
        res := '';
        read_tree( root_tree, res);
        P1:= res;
    end;

procedure postfix_calculate( str_line: string; c, s: integer; var stack: stack_type; var res: real);
    var
        tc, code, ti:integer;
        cal: real;
        ch:char;
    begin
        if c <= ord(str_line[0]) then
            begin
                ch := str_line[c];
                if ch in NUM then
                    begin
                        val(ch, ti, code);
                        stack[s] := ti;
                        postfix_calculate( str_line, c+1 ,s+1, stack, res);
                    end
                else
                    begin
                        dec(s);
                        case ch of
                            '+': cal := stack[s-1] + stack[s];
                            '-': cal := stack[s-1] - stack[s];
                            '/': cal := stack[s-1] / stack[s];
                            '*': cal := stack[s-1] * stack[s];
                        end;
                        stack[s-1] := cal;
                        postfix_calculate( str_line, c+1, s , stack, res);
                    end;
            end
        else
            begin
                res := stack[1];
            end;
        
    end;{END postfix_calculate}
function P2( str_line: string):real;
    var
        stack: stack_type;
        res :real;
    begin
        postfix_calculate( str_line, 1,1 , stack, res);
        P2:=res;
                        
    end;

function main:integer;
    const
        IN_PATH = 'lab5.in';
        OUT_PATH = 'lab5.out';
    var
        in_file, out_file: text;
		str_line:string;
        root_tree: utree_type;
    begin
        assign( in_file, IN_PATH);
		assign( out_file, OUT_PATH);
        reset( in_file);
        rewrite( out_file);
		readln( in_file, str_line);
    
        writeln(out_file,str_line);
        writeln(out_file,'-= Result =-');
        writeln(out_file,  P2( P1(str_line)):4);
        close(in_file);
        close(out_file);

    end;
begin
    main;
end.
