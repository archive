#include <stdio.h>
#include <stdlib.h>
#define PATH "test.data"
/* 
 * Apkawa <apkawa@gmail.com>
 */

int main ( int argc, char *argv[] )
{

    FILE *in_file;
    char line[256];
    int ch;
    int i = 0;

    in_file = fopen( PATH , "r");
    while( ( ch = fgetc( in_file )) != EOF ) {
        if (ch != '\n'){
            line[i] = (char) ch;
            i++;

        }
    }
    line[i] = '\0';
    printf("%s\n", line);
    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
