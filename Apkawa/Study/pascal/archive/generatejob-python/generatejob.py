#!/usr/bin/python
# -*- coding: utf-8 -*-
###
#This file is part of <name prog> project
#
#<описание программы>
#Copyright (C) <year> <name|nick>
#
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#You can contact author by email <my email>
###

import os
import random

dict_folder = os.path.join( os.path.abspath('.'), 'dict' )
dicts = {'#fam':open( os.path.join( dict_folder, 'family.txt') ,'r' ),
         '#name': open( os.path.join( dict_folder, 'name.txt'), 'r')}

def parse_argv( argv):
    result = {}
    for a in argv:
        if a.startswith('('):
            if a.endswith(')'):
                result.update( {a: a[1:-1].split(',') })
        if a.startswith('['):
            if a.endswith(']'):
                result.update( { a:range( *[ int(i) for i in a[1:-1].split('..')] ) } )
        if a.startswith('#fam'):
            result.update( {a:a} )
        if a.startswith('#name'):
            result.update( {a:a} )
    return result

def rand_chois_from_dict( tag = '#name'):
    f = dicts.get(tag)
    f.seek(0,2)
    max_pos = f.tell()
    f.seek( random.randint( 0L, max_pos) )
    f.readline()
    selected = f.readline().replace('\r','').replace('\n','')
    return selected

def main( argv=['#name','#fam','(м,ж)','[1500..8900]'], __max = 1024*2, delim=' ', delim_min_count=4,delim_max_count=10):
    parsed_argv = parse_argv( argv )
    result_array = []
    for i in xrange(__max):
        __temp = []
        for a in argv:
            _pa = parsed_argv.get(a)
            if _pa:
                if dicts.get(a):
                    __temp.append( rand_chois_from_dict( _pa ) )
                else:
                    __temp.append( str(random.choice( _pa )) )
        _delim = delim*random.randint( delim_min_count, delim_max_count)
        result_array.append( _delim.join( __temp ) )
    result = '\n'.join( result_array )
    print result


if __name__ == '__main__':
    main()

