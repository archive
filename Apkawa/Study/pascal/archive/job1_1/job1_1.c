#include    <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <wctype.h>
#define PATH "test.data"
#define ALPHA 1
#define SPACE 2
#define OTHER 3
#define START 4

/* 
 * Apkawa <apkawa@gmail.com>
 */

int main ( int argc, char *argv[] )
{
    FILE *in_file;
    int c;
    int word_count = 0;
    int alpha_count = 0;
    int space_count = 0;
    int state = START;
    in_file = fopen( PATH, "r"  );
    while ( state != EOF ){
        c = fgetc( in_file);
        // printf("%i ",c);
        switch (state){
            case ALPHA:
                         // printf("ALPHA\n");
                         if(isalpha(c)){ 
                             alpha_count++;
                             break;    
                         }
                         if(c == ' '){
                             state = SPACE;
                             word_count++;
                             space_count++;
                             break;
                         } 
                         if (c == EOF){
                             // printf("%i\n",state);
                             state = EOF;
                             break;
                         }
                         state = OTHER;

            case SPACE: 
                         // printf("SPACE\n");
                         if (c == ' '){ 
                             space_count++;
                         }
                         if (isalpha(c)){ 
                             state = ALPHA;
                             alpha_count++;
                             break;
                            }
                         if (c == EOF){
                             // printf("%i\n",state);
                             state = EOF;
                             break;
                         }
                         state = OTHER;
            case OTHER:
                         // printf("OTHER\n");
                         if (isalpha(c)){ 
                             state = ALPHA;
                             alpha_count++;
                             break;
                            }
                         if (c == ' '){ 
                             state = SPACE;
                             space_count++;
                             break;
                            }
                         if (c == EOF){
                             // printf("%i\n",state);
                             state = EOF;
                             break;
                         }
            default: 
                         if (isalpha(c)){ 
                             state = ALPHA;
                             alpha_count++;
                             break;
                            }
                         if (c == ' '){ 
                             state = SPACE;
                             space_count++;
                             break;
                            }
                         if (c == EOF){
                             // printf("%i\n",state);
                             state = EOF;
                             break;
                         }
                         state = OTHER;
        }
    }
    printf( "%i %i %i\n", word_count, alpha_count, space_count);



    return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
