(*This file is part of <name prog> project

job 1.1
Copyright (C) 2009 Apkawa

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

You can contact author by email apkawa@gmail.com
*)

{$MODE TP} (* compiler flag for fpc *)

program job_1_1;
{
Дан список группы в 10 человек в виде
ФАМИЛИЯ   ПОЛ   РЕЗУЛЬТАТЫ_СЕССИИ
15 симв           1 симв             4 симв
Данные в одной строке отделяются друг от друга пробелами.
Например:   
Иванов        м     4455 
Петрова       ж     3554
………………………….                                                                               
Определить лидеров среди мужчин и женщин по успеваемости и их средний балл (у мужчин и женщин отдельно).                                                                      


Список лидеров напечатать в виде:                                                           
Иванов	4455  ср.балл 4.5
Петрова 3554  ср.балл 4.3
Общий   средний балл мужчин равен   4.2
Общий средний балл женщин равен   4.1

}
const
    path = 'job1_1.data';

    ALPHA  = 1;
    NUMBER = 2;
    SPACE  = 3;
    OTHER  = 4;
    END_LINE = 5;
    END_FILE = 6;{26}

    START_STATE = 1;
    WATING_ALPHA = 2;
    WATING_NUMBER = 3;
    WATING_SPACE = 4;
    WATING_END_LINE = 5;
    END_STATE = 6;

    MAX_SIZE = 255;

type
    text_array = array [0..MAX_SIZE, 0..MAX_SIZE, 0..MAX_SIZE*2] of char;
    index_array = array [0..MAX_SIZE, 0..MAX_SIZE] of integer;
var
    state: integer; { 0,1,2,3 }
    symbol: integer;
    i,j : integer;
    line_pos, word_pos: integer;
    word_count, space_count, alpha_count, alpha_word_count: integer;
    ch: char;
    in_file:text;
    parsed_text: text_array ;
    index: index_array; 
begin
    assign( in_file, path );
    reset( in_file );
    i := 0;
    line_pos := 0;
    word_pos := 0;
    word_count :=0;
    space_count := 0;
    alpha_count := 0;
    alpha_word_count := 0;
    state := START_STATE;

    while true do
        begin
            read( in_file, ch);

            if ch in ['0'..'9'] then
                symbol := NUMBER
            else
                if ch in [chr(10), chr(13)] then
                    symbol := END_LINE
                else
                    if ch in [' '] then
                        symbol := SPACE
                    else
                        if eof(in_file) then
                            symbol := END_FILE
                        else
                            symbol := ALPHA;

            case state of
                START_STATE:
                    begin
                        case symbol of
                            ALPHA:
                                begin
                                    parsed_text[line_pos,word_pos,alpha_word_count] := ch;
                                    state := WATING_ALPHA;
                                end;
                            NUMBER: 
                                begin
                                    parsed_text[line_pos,word_pos,alpha_word_count] := ch;
                                    state := WATING_NUMBER;
                                    
                                end;
                            SPACE:
                                begin
                                    state := WATING_SPACE;
                                    
                                end;
                            END_LINE:
                                begin
                                    state := WATING_END_LINE;
                                    
                                end;
                            END_FILE:
                                begin
                                    state := END_STATE;
                                    
                                end;
                        end;
                    end;
                WATING_ALPHA:
                    begin
                        case symbol of
                            ALPHA:
                                begin
                                    alpha_word_count := alpha_word_count +1;
                                    parsed_text[line_pos,word_pos,alpha_word_count] := ch;
                                    state := WATING_ALPHA;
                                end;
                            NUMBER: 
                                begin
                                    alpha_word_count := alpha_word_count +1;
                                    parsed_text[line_pos,word_pos,alpha_word_count] := ch;
                                    state := WATING_NUMBER;
                                    
                                end;
                            SPACE:
                                begin
                                    index[line_pos,word_pos]:= alpha_word_count;
                                    word_pos := word_pos +1;
                                    word_count := word_count + 1;
                                    alpha_word_count := 0;

                                    state := WATING_SPACE;
                                end;
                            END_LINE:
                                begin
                                    index[line_pos,word_pos]:= alpha_word_count;
                                    word_count := word_count + 1;
                                    line_pos := line_pos +1;
                                    word_pos := 0;
                                    alpha_word_count := 0;

                                    state := WATING_END_LINE;
                                end;
                            END_FILE:
                                begin
                                    state := END_STATE;
                                end;
                        end;
                    end;
                WATING_NUMBER:
                    begin
                        case symbol of
                            ALPHA:
                                begin
                                    alpha_word_count := alpha_word_count +1;
                                    parsed_text[line_pos,word_pos,alpha_word_count] := ch;
                                    state := WATING_ALPHA;
                                    
                                end;
                            NUMBER: 
                                begin
                                    alpha_word_count := alpha_word_count +1;
                                    parsed_text[line_pos,word_pos,alpha_word_count] := ch;
                                    state := WATING_NUMBER;
                                    
                                end;
                            SPACE:
                                begin
                                    word_pos := word_pos +1;
                                    word_count := word_count + 1;
                                    alpha_word_count := 0;

                                    state := WATING_SPACE;
                                    
                                end;
                            END_LINE:
                                begin
                                    word_count := word_count + 1;
                                    line_pos := line_pos +1;
                                    alpha_word_count := 0;
                                    word_pos := 0;
                                    state := WATING_END_LINE;
                                    
                                end;
                            END_FILE:
                                begin
                                    index[line_pos,word_pos]:= alpha_word_count;
                                    word_count := word_count + 1;
                                    state := END_STATE;
                                    
                                end;
                        end;
                    end;
                WATING_SPACE:
                    begin
                        case symbol of
                            ALPHA:
                                begin
                                    parsed_text[line_pos,word_pos,alpha_word_count] := ch;
                                    state := WATING_ALPHA;
                                    
                                end;
                            NUMBER: 
                                begin
                                    parsed_text[line_pos,word_pos,alpha_word_count] := ch;
                                    state := WATING_NUMBER;
                                    
                                end;
                            SPACE:
                                begin
                                    state := WATING_SPACE;
                                    
                                end;
                            END_LINE:
                                begin
                                    line_pos := line_pos +1;
                                    word_pos := 0;
                                    alpha_word_count := 0;
                                    state := WATING_END_LINE;
                                    
                                end;
                            END_FILE:
                                begin
                                    state := END_STATE;
                                    
                                end;
                        end;
                    end;
                WATING_END_LINE:
                    begin
                        case symbol of
                            ALPHA:
                                begin
                                    parsed_text[line_pos,word_pos,alpha_word_count] := ch;
                                    state := WATING_ALPHA;
                                    
                                end;
                            NUMBER: 
                                begin
                                    parsed_text[line_pos,word_pos,alpha_word_count] := ch;
                                    state := WATING_NUMBER;
                                    
                                end;
                            SPACE:
                                begin
                                    state := WATING_SPACE;
                                    
                                end;
                            END_LINE:
                                begin
                                    state := WATING_END_LINE;
                                    
                                end;
                            END_FILE:
                                begin
                                    state := END_STATE;
                                    
                                end;
                        end;
                    end;
                
            end;

            if state = END_STATE then
                break
                
        end;
        close( in_file);
        writeln( line_pos );
        writeln( word_count, word_pos);
        for i:=0 to line_pos-1 do
            begin
            for j:=0 to 2 do
                write( parsed_text[i,j],' ');
            writeln;
            end;

end.
