#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys,os
'''
#/usr/bin/sh
    TEMPLATE='<?xml version="1.0" standalone="no"?>
    <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
      "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
    <svg width="800" height="600" version="1.1"
         xmlns="http://www.w3.org/2000/svg"
         xmlns:xlink="http://www.w3.org/1999/xlink">
      <rect width="800" height="600"
            style="fill:rgb(0,0,0);"/>
      <rect x="99" y="49" width="602" height="402"
            style="stroke:rgb(255,255,255);stroke-width:1;"/>
      <image x="100" y="50" width="600px" height="400px"
             xlink:href="%s"/>
      <text text-anchor="middle" x="50%%" y="510"
            font-family="Verdana" font-size="55"
            style="fill:rgb(255,255,255);">
        %s
      </text>
      <text text-anchor="middle" x="50%%" y="550"
            font-family="Verdana" font-size="30"
            style="fill:rgb(255,255,255);">
        %s
      </text>
    </svg>';
    printf "$TEMPLATE" "$1" "$2" "$3" | convert - "$4"
# ./generator.sh <картинка> <заголовок> <текст> <итоговая картинка.png>
'''
argv = sys.argv[1:]
print argv
_file, title, text, out = argv[0], argv[1], argv[2], argv[3]

template = '''<?xml version="1.0" standalone="no"?>
    <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
      "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
    <svg width="800" height="600" version="1.1"
         xmlns="http://www.w3.org/2000/svg"
         xmlns:xlink="http://www.w3.org/1999/xlink">
      <rect width="800" height="600"
            style="fill:rgb(0,0,0);"/>
      <rect x="99" y="49" width="602" height="402"
            style="stroke:rgb(255,255,255);stroke-width:1;"/>
      <image x="100" y="50" width="600px" height="400px"
             xlink:href="%s"/>
      <text text-anchor="middle" x="50%%" y="510"
            font-family="Verdana" font-size="55"
            style="fill:rgb(255,255,255);">
        %s
      </text>
      <text text-anchor="middle" x="50%%" y="550"
            font-family="Verdana" font-size="30"
            style="fill:rgb(255,255,255);">
        %s
      </text>
    </svg>'''%(_file,title,text)
print os.popen('echo "%s" | convert - %s'%(template, out)).read()








