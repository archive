# -*- coding: utf-8 -*-
import urwid.curses_display as urw_cd
import urwid
from torrentru import tsru

class ui:
    def __init__(self):
        if main().main():
            text = search().main()
            main(text,True).main()
            


    pass

class main:
    def __init__(self,text=None, flag=None):
        if not text:
            text = "h - help \ns - search" 
        instruct = 'Press h to help'
        self.botton = urwid.Frame(self.listbox(text,flag), footer = self.footer(instruct))

    def footer(self,text,flag=None):
        CONTENT = [ urwid.AttrWrap( w, None, 'reveal focus' ) for w in [
	urwid.Text("This is a text string that is fairly long"),
	urwid.Divider("-"),
	urwid.Text("Short one"),
	urwid.Text("Another"),
	urwid.Divider("-"),
	urwid.Text("What could be after this?"),
	urwid.Text("The end."),
] ]
        if not flag:
            return urwid.AttrWrap( urwid.Text(text), 'botton' )
        else: return [ urwid.AttrWrap( i, None, 'reval focus') for i in text]


    def listbox(self, text, flag=None):
        if not flag:
            items = urwid.SimpleListWalker([ urwid.Edit(('help',text))])
        else:
            items = urwid.SimpleListWalker([ urwid.Edit(('help',text)).edit_text])
        return urwid.ListBox(items)


    def main(self):

        self.ui = urw_cd.Screen()
        self.ui.register_palette([
            ('botton', 'black', 'dark cyan', 'underline'),
            ('help', 'default', 'default', 'bold'),
            ])
        return self.ui.run_wrapper( self.run )

    def run(self):
        cols, rows= self.ui.get_cols_rows()
        reply = None
        ask = urwid.Edit("Enter search word\n")
        while True:
            canvas = self.botton.render( (cols,rows), focus=True )
            self.ui.draw_screen( (cols,rows), canvas )
            keys = self.ui.get_input()
            for k in keys:
                if k == 'q':
                    break
                if k ==  's':
                    return True


class search:
    def __init__(self):
        self.ui = urw_cd.Screen()
    def main(self):
        self.ui.run_wrapper( self.run )

    def out(self, reply):
        ls=[]
        ts = tsru()
        repl = ts.search(reply)
        for i in repl:
            ls.append(urwid.Text(i))
            ls.append(urwid.Divider(i))
        return ls

    def run(self):
        cols, rows = self.ui.get_cols_rows()
        ask = urwid.Edit("Enter search word\n")
        fill = urwid.Filler( ask )
        reply = None
        while True:
            canvas = fill.render( (cols, rows), focus=True )
            self.ui.draw_screen( (cols, rows), canvas )

            keys = self.ui.get_input()
            for k in keys:
                if k == "enter":
                    reply = urwid.Text(ask.edit_text)
                    return self.out(reply)
                if fill.selectable():
                    fill.keypress( (cols, rows), k )

class result:
    def __init__(self,result):
        self.ui = urw_cd.Screen()
    def main(self):
        self.ui.run_wrapper( self.run )

    def run(self):
        cols, rows = self.ui.get_cols_rows()
        ask = urwid.Edit("Enter search word\n")
        fill = urwid.Filler( ask )
        reply = None
        while True:
            canvas = fill.render( (cols, rows), focus=True )
            self.ui.draw_screen( (cols, rows), canvas )

ui()
#main().main()
