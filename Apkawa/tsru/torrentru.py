# -*- coding: utf-8 -*-
import urllib2, httplib,re,sys, time
import optparse
from BeautifulSoup import BeautifulSoup as bfsoup
from BeautifulSoup import BeautifulStoneSoup as BfSS

'''
----------------------------
Имя пользователя: testdesu_2
Пароль: qazqaz
----------------------------
'''

USER_AGENT='Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.12) \
                        Gecko/20050922 Firefox/1.0.7 (Debian package 1.0.7-1)'

class tsru:
    def __init__(self):
        self.user = ''
        self.paswd = ''
        self.url = 'http://torrents.ru/forum'

    def auth(self):
        def get_cookie():
            body = 'login_username=%s&login_password=%s&autologin=1&login=%%C2%%F5%%EE%%E4'%(self.user, self.paswd)
            header = httplib.HTTPConnection('torrents.ru')
            header.putrequest('POST','/forum/login.php')
            header.putheader('Content-Length', str(len(body)))
            header.putheader('Referer', 'http://torrents.ru/forum/index.php')
            header.putheader('User-Agent', USER_AGENT)
            header.putheader('Content-Type','application/x-www-form-urlencoded')
            header.endheaders()
            header.send(body)
            self.cookie = header.getresponse().getheader('set-cookie')
            return self.cookie

        chk = None
        count = 0
        while not chk:
            sys.stderr.write('Попытка авторизации №%d\r'%count)
            count+=1
            time.sleep(5)
            chk = get_cookie()

    def get_html(self,url,size=None):
        req = urllib2.Request(url)
        req.add_header('Cookie', self.cookie)
        return urllib2.urlopen(req).read(size)

    def profile(self):
        stage1 = bfsoup(self.get_html('http://torrents.ru/forum/index.php'))
        stage1 = self.url+'/'+stage1.find('div',attrs = {'class':'topmenu'}).find('a',{'href':re.compile('profile.php\?mode=viewprofile')}).get('href')
        stage2 = BfSS(self.get_html(stage1))
        html_profile = stage2.find('table', {'class':'user_profile bordered w100'})
        print html_profile


    def test(self,file):
        f = open(file)
        html = f.read()
        f.close()
        return html

    def search(self, word):
        def parse(html):
            def extract(tBody):
                check = tBody.find(attrs = {'class':'row1 tCenter'})
                if check: check = check.find('span').get('class')
                else: check = '' #tBody.find(attrs = {'class':'row1 tCenter clickable tor-status-td'}).find('span',attrs = {'class':True})
                if check == 'tor-icon tor-not-approved' or check == 'tor-icon tor-approved':

                    temp = {
                        'topic_link': self.url+tBody.find(attrs = {'class':'genmed tLink'}).get('href')[1:],
                        'topic_text': tBody.find(attrs = {'class':'genmed tLink'}).find(text=True),
                        'status'    : check,
                        'author'    : tBody.find(attrs = {'class':'med'}).find(text=True),            #class='med'
                        'torrent'   : self.url+tBody.find(attrs = {'class':'med dLink'}).get('href')[1:],           #class='med dLink'
                        'size'      : tBody.find(attrs = {'class':'row4 small nowrap'}).find(text=True),              #class='row4 small nowrap'
                        'seed'      : tBody.find(attrs = {'class':'row4 seedmed'}).find(text=True),              #class='row4 seedmed'
                        'leech'     : tBody.find(attrs = {'class':'row4 leechmed'}).find(text=True),             #class='row4 leechmed'
                        'downloaded': tBody.find(attrs = {'class':'row4 small'}).find(text=True),        #class='row4 small'
                        'added'     : ' '.join([i.find(text=True) for i in tBody.find(attrs = {'class':'row4 small nowrap','title':True}).findAll('p')])
                            }

                    for i in temp.keys():
                        temp[i] = BfSS(temp[i],convertEntities='html')
                    return temp
                else: return False

            soup = bfsoup(html)
            tbody = soup.findAll('tbody')
            result = [extract(i) for i in tbody]
            return result

        #url=self.url+'/tracker.php'
        #quere = '%s?max=1&to=1&nm=%s'%(url,urllib2.quote(word))
        #html = self.get_html(quere)
        html = self.test('search.html')
        _parse = parse(html)
        return self.output(_parse)

    def output(self, result):
        count = 1
        out_format_base = ' | %s |\n[%s] S: [%s] L: [%s] D: [%s] A: [%s]\n %s\n'
        out_format_1 = '%d) %s'+out_format_base 
        out_format_2 = '%d) %.69s'+out_format_base 
        ls = []
        for i in result:
            if i:
                out = len(i['topic_text'].contents[0]) > 69 and out_format_2 or out_format_1 
                out = out%(count, 
                                                i['topic_text'],
                                                i['author'],
                                                i['size'],
                                                i['seed'],
                                                i['leech'],
                                                i['downloaded'],
                                                i['added'],
                                                i['topic_link']) 
                count+=1
                ls.append(out)
            else: pass
        return ls
                                                
def optparse(args):

	pass
    
if __name__ == '__main__':
    s = tsru()
    #s.auth()
    s.search(' '.join(sys.argv[1:]))
    #s.profile()



'''
----------------------------
Имя пользователя: geeqie_001
Пароль: qaz
----------------------------


Cookie: bb_data=a%3A3%3A%7Bs%3A2%3A%22uk%22%3BN%3Bs%3A3%3A%22uid%22%3Bi%3A314002%3Bs%3A3%3A%22sid%22%3Bs%3A20%3A%22dBI3Kxtsy5fJTOdS5iBi%22%3B%7D; bb_sid=dBI3Kxtsy5fJTOdS5iBi; bb_isl=0
Auth 

POST /forum/login.php HTTP/1.1
Host: torrents.ru
User-Agent: Mozilla/5.0 (X11; U; Linux i686; ru-RU; rv:1.9.0.2pre) Gecko/2008072703 Firefox/3.0.2pre (Swiftfox)
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: ru,en-us;q=0.7,en;q=0.3
Accept-Encoding: gzip,deflate
Accept-Charset: windows-1251,utf-8;q=0.7,*;q=0.7
Keep-Alive: 300
Proxy-Connection: keep-alive
Referer: http://torrents.ru/forum/viewforum.php?f=1813
Content-Type: application/x-www-form-urlencoded
Content-Length: 64
login_username=Apkawa&login_password=xerosexo&login=%C2%F5%EE%E4

search

POST /forum/tracker.php HTTP/1.1
Host: torrents.ru
User-Agent: Mozilla/5.0 (X11; U; Linux i686; ru-RU; rv:1.9.0.2pre) Gecko/2008072703 Firefox/3.0.2pre (Swiftfox)
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: ru,en-us;q=0.7,en;q=0.3
Accept-Encoding: gzip,deflate
Accept-Charset: windows-1251,utf-8;q=0.7,*;q=0.7
Keep-Alive: 300
Proxy-Connection: keep-alive
Referer: http://torrents.ru/forum/index.php
Cookie: bb_data=a%3A3%3A%7Bs%3A2%3A%22uk%22%3BN%3Bs%3A3%3A%22uid%22%3Bi%3A314002%3Bs%3A3%3A%22sid%22%3Bs%3A20%3A%22dBI3Kxtsy5fJTOdS5iBi%22%3B%7D; bb_sid=dBI3Kxtsy5fJTOdS5iBi; bb_isl=0
Content-Type: application/x-www-form-urlencoded
Content-Length: 24
max=1&to=1&nm=soul+eater

example

<tbody id="tor_11248324">
<tr class="tCenter">
<td class="row1"><img src="http://static.torrents.ru/templates/default/images/icon_minipost.gif" class="icon1" alt="post" /></td>
<td class="row1 tCenter" title="не проверено"><span class="tor-icon tor-not-approved">*</span></td>
<td class="row1"><a class="gen" href="tracker.php?f=1389">Аниме (основной подраздел)</a></td>
<td class="row4 med tLeft">
<div>
<a class="genmed tLink" href="./viewtopic.php?t=1024117"><b>Пожиратель душ / Soul Eater (Игараси Такуя) [JAP+SUB][2008,приключ<wbr>ения, комедия, фэнтези, сёнэн, TVrip]</wbr></b></a>
</div>
</td>
<td class="row1"><a class="med" href="tracker.php?pid=981210">a-f14</a></td>
<td class="row4 med nowrap"><a class="med dLink" href="./download.php?id=873028">[<span class="dlSp"> </span><span class="bold" onclick="this.className='normal'">DL</span><span class="dlSp"> </span>]</a></td>
<td class="row4 small nowrap">1.47&nbsp;GB</td>
<td class="row4 seedmed" title="Seeders"><b>4</b></td>
<td class="row4 leechmed" title="Leechers"><b>5</b></td>
<td class="row4 small">3</td>
<td class="row4 small nowrap" style="padding: 1px 3px 2px;" title="Добавлен">
<p>23:29</p>
<p>8-Сен-08</p>
</td>
</tr>
</tbody>


def utf8_cp1251(text):
    u=unicode(text, 'utf8')
    cyrillic_cp1251=u.encode('cp1251')
    return cyrillic_cp1251
def cp1251_utf8(text):
    cyrillic_cp1251=unicode(text, 'cp1251')
    text=cyrillic_cp1251.encode('utf8')
    return text

console color
if [ "$USECOLOR" = "YES" -o "$USECOLOR" = "yes" ]; then
	C_MAIN="\033[1;37;40m"      # main text
	C_OTHER="\033[1;34;40m"     # prefix & brackets
	C_SEPARATOR="\033[1;30;40m" # separator

	C_BUSY="\033[0;36;40m"      # busy
	C_FAIL="\033[1;31;40m"      # failed
	C_DONE="\033[1;37;40m"      # completed
	C_BKGD="\033[1;35;40m"      # backgrounded

	C_H1="\033[1;37;40m"        # highlight text 1
	C_H2="\033[1;36;40m"        # highlight text 2

	C_CLEAR="\033[1;0m"
'''
