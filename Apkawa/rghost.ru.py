#!/usr/bin/python
# -*- coding: utf-8 -*-
import httplib, mimetypes, sys
USER_AGENT = 'Mozilla/5.0 (X11; U; Linux i686; ru-RU; rv:1.9.0.2pre) Gecko/2008072703 Firefox/3.0.2pre (Swiftfox)'

def get_file_contents(file_name):
    f = open(file_name);
    data = f.read()
    f.close()
    return data

def get_content_type(file_name):
    cont_type=mimetypes.guess_type(file_name)[0] or 'application/octet-stream'
    return cont_type

def encode_multipart_formdata(filename):
    BOUNDARY = '----------'
    CRLF = '\r\n'
    L = []
    # add additional form fields
    form_vaule = {'authenticity_token':'13619ba0aed720f2c822937f14409450d658b50c'}

    L.append('--' + BOUNDARY)
    L.append('Content-Disposition: form-data; name="authenticity_token"')
    L.append('')
    L.append('13619ba0aed720f2c822937f14409450d658b50c')
    L.append('--' + BOUNDARY)
    L.append('Content-Disposition: form-data; name="file"; filename="%s"' \
            % (filename))
    L.append('Content-Type: %s' % get_content_type(filename))
    L.append('')
    L.append(get_file_contents(filename))
    L.append('--' + BOUNDARY + '--')
    L.append('')
    body = CRLF.join(L)
    content_type = 'multipart/form-data; boundary=%s' % BOUNDARY
    return content_type, body


def upload_file(filename):
    cookie = '_rghost_session=BAh7CDoMY3NyZl9pZCIlMjYzMjY4MzBiNGUwNzE0NWI3ODBlODhmNmNmYTk1ZTgiCmZsYXNoSUM6J0FjdGlvbkNvbnRyb2xsZXI6OkZsYXNoOjpGbGFzaEhhc2h7AAY6CkB1c2VkewA6CWxhbmciCnJ1LVJV--bd4e9c60b4a8ccb5a6b9f797bdc07a4265150872'
    content_type, body = encode_multipart_formdata(filename)
    header = httplib.HTTPConnection('phonon.rghost.ru')
    '''
    f = open(filename)
    f.seek(0,2)
    size = int(f.tell())
    f.seek(0)
    '''
    header.putrequest('POST', '/files')
    header.putheader('User-Agent', USER_AGENT)
    header.putheader('Cookie',cookie)
    header.putheader('Content-Type', content_type)  
    header.putheader('Content-Length', str(len(body)))
    #header.putheader('Content-Length', str(len(body)+size+18))
    header.endheaders()
    #header.set_debuglevel(1)
    header.send(body)
    '''
    #while True:
    #    try:
    #        header.send(f.next())
    #    except: break
    #header.send( get_file_contents(filename))
    #header.send('\r\n--------------\r\n')
    '''
    return header.getresponse()

if __name__ == '__main__':
    filename = sys.argv[1]
    if filename:
        print upload_file(filename).getheader('location')
    else: print 'Error'


