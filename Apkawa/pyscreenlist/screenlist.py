#!/usr/bin/python
# -*- coding: utf-8 -*-
###
#This file is part of screenlist project
#
#screenlist is a program that captures frames from a video file and save these frames as thumbnails in a single image file.
#Copyright (C) 2009 Apkawa
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#You can contact author by email Apkawa@gmail.com
###

import os
import shutil
import tempfile

import re
import time
import Image, ImageFont, ImageDraw, ImageOps
from math import ceil


import optparse


#http://code.google.com/p/mplayer-snapshot/
#http://code.activestate.com/recipes/412982/
# TODO:
# get info file - mediainfo
# get screen - http://code.google.com/p/pyffmpeg/
# ffmpeg -i video.avi -an -ss 00:00:10 -r 1 -vframes 1 -s 320×240 -y -f mjpeg screenshot.jpg

def test(__s):
    s = __s()
    s.open_video('01.mkv')
    #s.make_screens('tmp')
    s.SETTING.title.logo = True
    s.make_screenlist(s.GRID,grid=(6,6),)
    s.save_result('tmp/s.png')

def human_format(num, suffix='b'):
    num=float(num)
    for x in ['','K','M','G','T']:
        if num<1024:
            return "%3.1f%s%s" % (num, x, suffix)
        num /=1024

def calc_resize( orig_wxh, w=None, h=None ):
    def ince(__float):
        return int( ceil( __float ))
    o_w, o_h = orig_wxh
    if w:
        h= ince( ( o_h * w ) / float(o_w) )
    if h:
        w = ince( ( o_w * h ) / float(o_h) )
    return (w, h)

#@test
class Screenlist:
    FRAMES = 0
    SECONDS = 1
    GRID = 2

    SETTING = type('Visual_Setting',(),{
        'canvas':type('Canvas_Setting',(),{
            'bgcolor':'#FFF',
            'fgcolor':'#000',
            'padding':5,
            }),
        'frame': type('Frame_Setting',(),{
            'bgcolor':'#CCC',
            'fgcolor':'#000',
            'fontsize':12,
            'padding':5,
            'border_color': '#AAA'
            }),
        'title': type('Title_Setting',(),{
            'fgcolor':'#000',
            'fontsize':18,
            'logo': False,
            'logopath': 'logo.png',
            }),
        '__doc__': 'Setting Screenlist class',
        })

    imgs = []
    temp_files =[]
    columns = None
    tempdir = tempfile.gettempdir()
    oformat = 'png'#jpeg
    def __init__(self):
        self.exec_mplayer_info = 'mplayer "%s"  \
                -ao null -vo null \
                -frames 1 \
                -identify \
                2>/dev/null'
        self.exec_mplayer = 'mplayer "%(videopath)s"  \
                    -ao null \
                    -vo %(oformat)s:outdir=%(tempdir)s \
                    -nojoystick \
                    -nolirc \
                    -nocache \
                    -nortc \
                    -noautosub \
                    -slave \
                    -benchmark\
                    -ss %(time_pos)s \
                    -frames 1 '
                    #2>/dev/null'
        self.exec_mplayer_thumb = self.exec_mplayer+ '-zoom -xy %(t_width)i '

    def open_video(self, video_path):
        'Open video and return bool'
        self.videopath = video_path
        __info = os.popen( self.exec_mplayer_info% video_path).read()
        #print __info
        self.identify = dict(re.findall('ID_(.*)=(.*)',__info))
        self.identify.update({
                    'filesize': os.stat( video_path).st_size
                })
        if self.identify.get('LENGTH'):
            #print self.identify
            self.video_info = re.findall('VIDEO\:.*', __info)[0]
            _a = re.findall('AUDIO\:.*',__info)
            self.audio_info = [ _a[0] if _a else 'AUDIO: None']

            for i in xrange(10):
                __a_lang = self.identify.get('AID_%i_LANG'%i)
                __a_name = self.identify.get('AID_%i_NAME'%i)
                if __a_lang:
                    __a_str = 'AID %i: %s %s'%(i, __a_lang, __a_name if __a_name else '')
                    self.audio_info.append( __a_str )
                else:
                    break
            #print self.video_info
            #print self.audio_info


            self.video_length = float(self.identify['LENGTH'])
            return True
        else:
            return False

        #print self.identify

    def get_screen(self, outfile, time_pos='00:00:00', t_width=None):
        __exec_arg = {
            'videopath':self.videopath,
            'time_pos':time_pos,
            'tempdir':self.tempdir,
            'oformat':self.oformat,
            }
        if t_width:
            __exec_arg.update({'t_width':t_width,})
            __t = os.popen(
                    self.exec_mplayer_thumb%__exec_arg+'2>/dev/null'
                        ).read()
        else:
            __t = os.popen(
                    self.exec_mplayer%__exec_arg+'2>/dev/null'
                        ).read()
        screen = os.path.join(self.tempdir,'00000001.png')
        try:
            shutil.copyfile( screen, outfile)
            os.remove( screen)
            return True
        except ( OSError,IOError):
            return False

    def make_screens(self, outdir,frames=5, prefix='',suffix='' , t_width=None):
        if not os.path.isdir( outdir):
            raise Exception('%s not dir'%outdir)

        s_time = int( self.video_length / frames )
        files = []
        for f in xrange(frames):
            __filepath = os.path.join(
                    os.path.abspath(outdir),
                    '%s%s%s.%s'%(prefix, f+1, suffix, self.oformat) )
            time_pos = self.str_time(
                    s_time*f if f else s_time/3
                    )
            if self.get_screen( __filepath, time_pos, t_width ):
                files.append(__filepath)
        return files

    def make_screenlist(self,
            time_type=FRAMES,#
            frames=24,
            grid=(6,6),
            columns=6,
            s_time=120,
            t_width=200,
            ):
        '''
        time_type - set method get frames of video.
            self.FRAMES, self.SECONDS, self.GRID
        t_width - width frame thumbinail
        '''

        padding = self.SETTING.canvas.padding
        fgcolor = self.SETTING.canvas.fgcolor
        bgcolor = self.SETTING.canvas.bgcolor


        if time_type == self.FRAMES:
            self.frames = frames
            s_time = int( self.video_length /frames)

        elif time_type == self.SECONDS:
            self.frames = int( self.video_length/ s_time )

        elif time_type == self.GRID:
            self.frames = grid[0]*grid[1]
            s_time = int( self.video_length/ self.frames )
            columns = grid[0]

        for f in xrange(self.frames):
            __tempfile = os.path.join( self.tempdir,'%i.png'%f)
            time_pos = self.str_time(
                    s_time*f if f else s_time/3
                    )

            #print time_pos

            if self.get_screen( __tempfile, time_pos, t_width):
                self.temp_files.append( __tempfile )
                self.imgs.append(
                        self._make_frame( Image.open( __tempfile ), time_pos ) )
            else:
                self.frames -=1
                continue
        #print self.frames

        def title( img, text_tuple,font=None, title_height=150, ):
            logo = self.SETTING.title.logo
            logo_left = 0
            if logo:
                logo_img = Image.open( self.SETTING.title.logopath)
                logo_img = logo_img.resize( calc_resize( logo_img.size, h=title_height-padding*2) )
                l_w,l_h = logo_img.size
                left = marl
                right = left+l_h
                top = mart
                bottom = top+l_h
                bbox = (left, top, right, bottom)
                img.paste( logo_img, bbox)

                logo_left = marl+l_w+padding


            fgcolor= self.SETTING.title.fgcolor
            if not font:
                font, fontsize = self._font()

            draw = ImageDraw.Draw( img)

            for r in xrange(len( text_tuple)):
                t = text_tuple[r]
                draw.text(
                        xy=(marl+logo_left ,mart+r*(padding+fontsize)),
                        text=t,
                        font=font,
                        fill=fgcolor )

        title_h = 150
        photow, photoh = self.imgs[0].size
        #print photow, photoh
        marl = 5
        marr = 5
        mart = 5
        marb = 5

        marw = marl+marr
        marh = mart+marb
        padw = (columns-1)*padding
        rows = int(ceil(self.frames/float(columns)))
        padh = (rows-1)*padding
        image_size = ( columns*photow+padw+marw, rows*photoh+padh+marh+title_h )

        self.image_new = Image.new('RGB', image_size, bgcolor)

        title_text = [
                'Filename: %s; '%os.path.split( self.identify['FILENAME'] )[1] +
                'size: %s'% human_format( int(self.identify['filesize']) ),
#                'Resolution: %sx%s; '%( self.identify['VIDEO_WIDTH'],self.identify['VIDEO_HEIGHT'])+
                'Duration: %s'%self.str_time( self.identify['LENGTH']) ,
                self.video_info,
                ]
        title_text += self.audio_info

        title( self.image_new, title_text)

        for r in xrange(rows):
            for c in xrange( columns):
                left = marl+c*(photow+padding)
                right = left+photow
                upper = (mart+r*(photoh+padding))+title_h
                lower = upper+photoh
                bbox = (left, upper, right, lower)
                try:
                    img = self.imgs.pop(0)
                    self.image_new.paste(img,bbox)
                except IndexError:
                    break

        self._clean()

    def str_time(self, seconds):
        'Return str_time in %H:%M:%S format'
        return time.strftime("%H:%M:%S", time.gmtime(
                    int( float(seconds) )
                ))

    def _make_frame(self, img, str_time,):
        __frame = self.SETTING.frame
        padding = __frame.padding
        bgcolor = __frame.bgcolor
        fgcolor = __frame.fgcolor
        border_color = __frame.border_color
        font, fontsize = self._font( self.SETTING.frame.fontsize)
        title_frame_height= fontsize + padding

        img = ImageOps.expand(img,(padding,padding,padding,title_frame_height),fill=bgcolor)
        img = ImageOps.expand(img,(1,1,1,1),fill= border_color )
        img_w, img_h = img.size
        _draw = ImageDraw.Draw( img)
        _draw.text(
                xy=( padding, img_h-(title_frame_height) ),
                text=str_time,
                font=font,
                fill= fgcolor)
        return img

    def _font(self, fontsize=18, fontpath='fonts/arial.ttf'):
        try:
            font = ImageFont.truetype( fontpath,fontsize)
        except IOError:
            font  = ImageFont.load_default()
        return font, fontsize

    def save_result(self,dest='tmp/screenlist.png'):
        self.image_new.save(dest, 'PNG')

    def _clean(self):
        for tf in self.temp_files:
            try:
                os.remove(tf)
            except OSError:
                pass
        return True

class CliAppScreenlist:
    time_type = None
    outdir = None

    frames=None
    seconds=None
    grid=None
    screenshots = False

    def __init__(self):
        pass
    def parseopt(self):
        def time_parse(option, opt_str, value, __parser, *args, **kwargs):
            if not self.time_type:
                self.time_type = kwargs.get('time_type')
            else:
                parser.error('select only of -f,-s,-g')
            if self.time_type == Screenlist.FRAMES:
                self.frames= int(value)
            elif self.time_type == Screenlist.SECONDS:
                self.seconds = int(value)
            elif self.time_type == Screenlist.GRID:
                self.grid = re.findall('^(\d+),(\d+)$',value)
                if self.grid:
                    self.grid = [int(i) for i in self.grid[0]]
                else:
                    parser.error('not valid values in %s. \d+,\d+'%option)
            if opt_str == '--sh':
                if value:
                    print dir(value)
            print option, opt_str, value, args, kwargs
            return 1

        parser = optparse.OptionParser()
        parser.add_option('-f','--frames', action='callback', type='int',
                callback=time_parse, callback_kwargs={'time_type':Screenlist.FRAMES})
        parser.add_option('-s','--seconds', action='callback', type='int' ,
                callback=time_parse, callback_kwargs={'time_type':Screenlist.SECONDS})
        parser.add_option('-g','--grid', action='callback', type='string', metavar='cols,rows',
                callback=time_parse, callback_kwargs={'time_type':Screenlist.GRID})
        parser.add_option('-o','--outdir', action='store',
                type='string',dest='outdir',)
        parser.add_option('--sh','--screenshots', action='store_true',dest='screenshots')
        if os.sys.argv[1:]:
            (options, self.videofiles ) = parser.parse_args()
            #print options, self.videofiles
            if not self.videofiles:
                parser.error('not select videofiles')
            if options.outdir:
                self.outdir = os.path.abspath( options.outdir )
                if not os.path.isdir(self.outdir):
                    parser.error('-o select dir name')
            if options.screenshots:
                self.screenshots = True
        else:
            parser.print_help()
            os.sys.exit(1)

    def main(self):
        screenlist = Screenlist()
        #screenlist.SETTING.title.logo = True
        for vf in self.videofiles:
            if os.path.isfile( vf):
                if not screenlist.open_video(vf):
                    os.sys.stderr.write( 'Not open file %s\n'%vf)
                    continue

                if self.time_type:
                    screenlist.make_screenlist(
                            self.time_type,
                            frames=self.frames,
                            s_time=self.seconds,
                            grid=self.grid)
                else:
                    screenlist.make_screenlist( screenlist.GRID )
                video_dir, video_name = os.path.split(vf)
                if not self.outdir:
                    save_dir = video_dir
                else:
                    save_dir= self.outdir
                save_dir=os.path.abspath( save_dir)
                result_path = os.path.join( save_dir, video_name+'.png')

                if self.screenshots:
                    files = screenlist.make_screens( save_dir, prefix='screenshots_'+video_name+'_')
                    print '\n'.join(files)

                if os.path.exists( result_path):
                    i=1
                    while True:
                        if os.path.exists( result_path):
                            result_path = os.path.join(
                                    save_dir, video_name+'-%i.png'%i)
                            i += 1
                        else:
                            break

                print result_path
                screenlist.save_result( result_path )







if __name__ == '__main__':
    c = CliAppScreenlist()
    c.parseopt()
    c.main()


