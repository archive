# -*- coding: utf-8 -*-
import urllib
from json import loads

class GoogleSearchApi:
    class Response:
        class Data:
            def __init__(self, resp_data):
                self.url = resp_data['url']
                self.visibleUrl = resp_data['visibleUrl']
                self.unescapedUrl = resp_data['unescapedUrl']
                self.cacheUrl = resp_data['cacheUrl']
                self.title = resp_data['title']
                self.titleNoFormatting = resp_data['titleNoFormatting']
                self.content = resp_data['content']
        class Cursor:
            def __init__(self):
                pass

        def __init__(self, resp):
            response = loads( resp)
            self.Status = response['responseStatus']
            self.DataCursor = response['responseData']['cursor']
#self.DataResults = response['responseData']['results']
            self.DataResults = [self.Data(res) for res in response['responseData']['results']]

    def __init__(self):
        self.api_url = 'http://ajax.googleapis.com/ajax/services/search/%(type)s?%(query)s'
        self.query = {'v':1.0, 'rsz':'small','hl':'en'}
        pass
    def __get_result(self, type):
        search_url = self.api_url%{'type':type,'query': urllib.urlencode( self.query) }
        resp = urllib.urlopen(  search_url ).read() 
        self.resp = self.Response( resp)
        
        return self.resp
            
    def __parse(self, resp):
        response = loads( resp)
        self.status = response['responseStatus']
        self.cursor = response['responseData']['cursor']
        self.results = response['responseData']['results']
    def __check_args(self, args, const):
        c_a_key = const.keys()
        result_args = {}

        for a in args.keys():
            if a in c_a_key:
                val = const[a]['val']
                if val:
                    if args[a] in val.keys():
                        result_args.update( {a: args[a]} )
                

    def search(self, word, lang='en'):
        self.query['q'] = urllib.quote(word)
        self.query['hl'] = lang
        return True
    def web(self, help=True, **args):
        const_arguments = {
                'cx': {
                    'desc':'This optional argument supplies the unique id for the Custom Search Engine that should be used for this request (e.g., cx=000455696194071821846:reviews).',
                    'val': None,},
                'cref': {
                    'desc':'This optional argument supplies the url of a linked Custom Search Engine specification that should be used to satisfy this request (e.g., cref=http%3A%2F%2Fwww.google.com%2Fcse%2Fsamples%2Fvegetarian.xml).',
                    'val':None},
                'safe':{
                    'desc':'This optional argument supplies the search safety level which may be one of:',
                    'val':{'active':'enables the highest level of safe search filtering',
                        'moderate':'enables moderate safe search filtering (default)',
                        'off':'disables safe search filtering'},},
                'lr':{
                    'desc':'This optional argument allows the caller to restrict the search to documents written in a particular language (e.g., lr=lang_ja). This list contains the permissible set of values.',
                    'val':None},
                }
                
#self.check_args( args, const_arguments )


#self.query.update( {'lr':'lang_'+lr, 'safe': safe })
        return self.__get_result('web')
    
    '''
    def image(self, help=False, **args ):# imgz = None, imgc=None, imgtype=None, as_filetype=None, as_sitesearch=None):
        arguments = {
            'imgz':{
                'desc':'This optional argument tells the image search system to restrict the search to images of the specified size, where size can be one of',
                'val':{
                    'icon':'restrict to small images',
                    'small|medium|large|xlarge':'restrict to medium images',
                    'xxlarge':'restrict to large images', 
                    'huge':'restrict to extra large images'
                },
            'imgc':{},
            'imgtype':{}, 
            'as_filetype':{},
            'as_sitesearch':{},
            'lr':{},}
#self.query.update( {'lr':'lang_'+lr, 'safe': safe })
        return self.__get_result('image')
    '''
        

def google(word):
    def web(results):
        if results:
            url = urllib.unquote(results[0]['url'])
            title1 = results[0]['titleNoFormatting']
            title2 = results[0]['title']
            content = re.sub('(<b>)|(</b>)','',results[0]['content'])
            text = '%s\n%s\n%s'%(title1,content,url)
            text = re.sub('&#39;','\'',unescape(text))
            #print text

            extra = '''<a href="%s">%s</a>
            <p>%s</p>
            '''%(url,title2,content)
            return True, text, extra
        else: return True, 'Nyaaa... Ничего не нашла.', ''
    def images(results):
        if results:
            imgurl = results[0]['unescapedUrl']
            return imgurl
        else: return None
    #http://code.google.com/apis/ajaxsearch/documentation/reference.html#_intro_fonje
    _type = 'images'
    #word = urllib.quote(word.encode('utf-8'))
    word = urllib.quote(word)
    src =  urllib.urlopen('http://ajax.googleapis.com/ajax/services/search/%s?v=1.0&q=%s'%(_type,word)).read()
    convert = loads(src)
    results = convert['responseData']['results']
    return images(results)
    #if type == 'web':
        #return web(results)
    #if type == 'images':
   #     return images(results)

if __name__ == '__main__':
    search = GoogleSearchApi()
    search.search('nya')
    result = search.web()
    print result.DataResults[0].url
