import socket
import sys

import urllib2
import re
PROXY = 'http://127.0.0.1:8118'
USER_AGENT = "Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5"
def test_proxy():
    proxy_handler = urllib2.ProxyHandler({'http': PROXY})
    req = urllib2.build_opener(proxy_handler)
    req.add_headers = [("User-Agent",USER_AGENT)]
    o = req.open( "http://checkip.dyndns.com/")
    print "Tor proxy:" ,re.findall("<body>(.*?)</body>",o.read())[0]

TOR_IP = '127.0.0.1'
TOR_PORT = 9051

AUTH_MSG = "authenticate \n"
CHANGE_MSG = "signal newnym \n"

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect((TOR_IP, TOR_PORT))

test_proxy()
sock.send(AUTH_MSG)
print sock.recv(512)
sock.send(CHANGE_MSG)
print sock.recv(512)


sock.close()

test_proxy()
