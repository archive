#!/usr/bin/python
# -*- coding: utf-8 -*-
###
#This file is part of <name prog> project
#
#<описание программы>
#Copyright (C) <year> <name|nick>
#
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#You can contact author by email <my email>
###
import os
import re
import time
import random
import urllib

URL = "http://g.e-hentai.org/"#?f_shash="
SHA1SUM_BIN = '/usr/bin/sha1sum'

def get_sha1_from_file(filepath):
    if os.path.isfile(filepath) and os.path.splitext(filepath)[1].lower() in ['.png','.jpg','.jpeg']:
        cmd = '%s "%s"' % (SHA1SUM_BIN, filepath)
        sha1_res = os.popen( cmd).read()
        m = re.search("^([\w]{40})[\s]", sha1_res)
        if m:
            return m.groups()[0]
    return None
def find_by_sha1(sha1):
    time.sleep(4)
    query = urllib.urlencode( {'f_shash':sha1} )
    _url = "%s?%s"%(URL, query)
    u = urllib.urlopen( _url)
    src = u.read()
    test_res = re.findall("class=\"(gtr0)\"", src)
    return len(test_res)


def main( *args, **kwargs):
    args = os.sys.argv[1:]
    for arg in args:
        if os.path.isfile(arg):
            sha1= get_sha1_from_file( arg )
            print find_by_sha1(sha1), " duplicate for ", arg
        elif os.path.isdir(arg):
            for dirpath, dirnames, filenames in os.walk(arg):
                for f in filenames:
                    filename = random.choice(filenames)
                    if os.path.splitext(filename)[1].lower() in ['.png','.jpg','.jpeg']:
                        filepath = os.path.join(dirpath,filename)
                        sha1= get_sha1_from_file( filepath )
                        for i in range(3):
                            dup_count = find_by_sha1(sha1)
                            if dup_count:
                                break
                        print dup_count, " duplicate for ", dirpath
                        break




if __name__ == "__main__":
    main()
